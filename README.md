# Project Git-stat

Desktop GUI za Git koji podržava rad sa više repozitorijuma, i osnovne
vizualizacije nad Git repoima.

- Korisnik na svojoj radnoj površini može da vidi više Git 
  repozitorijuma sa osnovnim informacijama o svakom.
- Pregled vremenske linije commitova po autoru, koji commitovi su kad 
  napravljeni, koji fajlovi su izmenjeni i broj izmenjenih linija.
- Čuvanje rezultata obrade za ponovnu upotrebu pri narednim pokretanjima.
- Mogućnost dodavanja i čuvanja komentara za sve značajne entitete - projekte,
  autore, komitove i mogućnosti generisanja izveštaja na osnovu tih beleški.
- Preuzimanje privatnih repozitoijuma, uz pristupni token.
- Slanje mejlova odabranim autorima.

## Zavisnosti
- Qt6, sa modulima Core, Widgets, Network i Test
- CMake, verzija 3.16 ili novija
- g++, testirano sa 9.3.0 i novijim

## Izgradnja
Izgradnja projekta:
- `cmake .`
- `make`
 
Pokretanje projekta:
- `./GitStat`

Pokretanje testova:
- `./runTests`

## Demo snimak
- [Link ka snimku](https://drive.google.com/file/d/13NvQm5XAyagKRREk6UZsG-zv8yxygVAf/view?usp=sharing)

## Dijagram klasa
![Dijagram klasa](./specifikacija/DijagramKlasa.svg)

## Developers

- [Luka Jovičić, 38/2018](https://gitlab.com/luka-j)
- [Jelena Keljać, 106/2018](https://gitlab.com/jkeljac)
- [Aleksandra Pešić, 15/2018](https://gitlab.com/caca20)
- [Katarina Dimitrijević, 27/2018](https://gitlab.com/KatarinaDimitrijevic)
- [Mirjana Jočović, 135/2018](https://gitlab.com/mirjanaa)
