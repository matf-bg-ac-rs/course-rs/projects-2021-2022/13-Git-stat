#ifndef DATAPROCESSINGTEST_H
#define DATAPROCESSINGTEST_H


#include <catch2/catch_test_macros.hpp>
#include "include/data/DataProcessing.h"
#include <QFileInfo>

TEST_CASE("DataProcessing::createReport","[function]") {
    SECTION("When function is called, if file with name of the given repo doesn't exist, it will be created with the name of repository") {

        QString fileName ="Repo1.txt";
        QFile report(fileName);
        QFileInfo check_file(fileName);
        if (check_file.exists() && check_file.isFile()) {
            report.remove();
        }
        bool expected = true;
        QString filePath = "";
        DataProcessing *dataProcessor = DataProcessing::getInstance();
        QString dirPath = dataProcessor->FILE_PATH +"/";
        QSharedPointer<QVector<QSharedPointer<Repo>>> copyDisk = dataProcessor->onDisk;

        QSharedPointer<Repo> r1 = QSharedPointer<Repo>::create(31055519,"Repo1",QDateTime::fromString("2021-11-04T20:02:21",Qt::ISODate),"www.url.com","",QDateTime::fromString("2021-12-25T14:08:49",Qt::ISODate));
        QVector<QSharedPointer<Repo>> inputRepos = QVector<QSharedPointer<Repo>>{r1};
        QSharedPointer<QVector<QSharedPointer<Repo>>> testRepos = QSharedPointer<QVector<QSharedPointer<Repo>>>::create(inputRepos);

        dataProcessor->setReposForReportTest(testRepos);
        dataProcessor->setFileNameForReportTest(filePath);
        dataProcessor->createReport(31055519);
        dataProcessor->setReposForReportTest(copyDisk);
        dataProcessor->setFileNameForReportTest(dirPath);

        QFileInfo checkIfExists(fileName);
        bool returnValue;
        if (checkIfExists.exists() && checkIfExists.isFile()) {
            returnValue = true;}
        else{
            returnValue = false;
        }
        REQUIRE(expected == returnValue);
    }

    SECTION("When function is called, if file with given repo name exists it won't be created again") {
        QString fileName ="Repo1.txt";
        QFile report(fileName);
        report.open(QIODevice::OpenModeFlag::WriteOnly | QIODevice::Text);
        report.close();
        DataProcessing *dataProcessor = DataProcessing::getInstance();
        QSharedPointer<QVector<QSharedPointer<Repo>>> copyDisk = dataProcessor->onDisk;
        QSharedPointer<Repo> r1 = QSharedPointer<Repo>::create(31055519,"Repo1",QDateTime::fromString("2021-11-04T20:02:21",Qt::ISODate),"www.url.com","",QDateTime::fromString("2021-12-25T14:08:49",Qt::ISODate));
        QVector<QSharedPointer<Repo>> inputRepos = QVector<QSharedPointer<Repo>>{r1};
        QString filePath = "";
        QString dirPath = QDir::homePath().append("/").append("Git-stat/");
        bool expected = true;
        QSharedPointer<QVector<QSharedPointer<Repo>>> testRepos = QSharedPointer<QVector<QSharedPointer<Repo>>>::create(inputRepos);

        dataProcessor->setReposForReportTest(testRepos);
        dataProcessor->setFileNameForReportTest(filePath);
        dataProcessor->createReport(31055519);
        dataProcessor->setReposForReportTest(copyDisk);
        dataProcessor->setFileNameForReportTest(dirPath);

        QFileInfo checkIfExists(fileName);
        //todo fix
        bool returnValue;
        if (checkIfExists.exists() && checkIfExists.isFile()) {
            returnValue = true;}
        else{
            returnValue = false;
        }
        REQUIRE(expected == returnValue);
    }
}

TEST_CASE("DataProcessing::removeRepo","[function]"){

    SECTION("If file doesn't have info about given repo, there are no changes"){
        QString fakePath = "fakeRepos.json";
        QFile repos(fakePath);
        repos.open(QIODevice::OpenModeFlag::WriteOnly | QIODevice::Text);
        QTextStream stream(&repos);
        stream << "[\n {\n"
                  "\"commitsPerAuthor\": [], "
                  "\"createdAt\": \"2021-11-04T20:02:21\",\n"
                  "            \"description\": \"\",\n"
                  "                    \"groupId\": 14033104,\n"
                  "                    \"groupName\": \"Projects 2021-2022\",\n"
                  "                    \"id\": 123123123,\n"
                  "                    \"lastActivity\": \"2021-12-27T15:44:32\",\n"
                  "                    \"name\": \"MATF / course-rs / Projects 2021-2022 / 13-Git-stat\",\n"
                  "                    \"numOfAuthors\": 11,\n"
                  "                    \"numOfCommits\": 40,\n"
                  "                    \"webUrl\": \"https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2021-2022/13-Git-stat\"\n"
                  "        }\n]";

         repos.close();
        int expected = 1;

        DataProcessing *dc = DataProcessing::getInstance();
        dc->setFileNameForTests(fakePath);
        dc->removeRepo(31055551);
        repos.open(QIODevice::OpenModeFlag::ReadOnly | QIODevice::Text);
        QByteArray jsonRepos =  repos.readAll() ;
        repos.close();
        QString dirPath = dc->FILE_PATH;
        dc->setFileNameForTests(dirPath.append("/repos.json"));
        QJsonArray readReposArray = QJsonDocument::fromJson(jsonRepos).array();
        int size = readReposArray.size();

        REQUIRE(expected == size);
    }

    SECTION("If file doesn't have any repos at all, there are no changes"){

        QString fakePath = "fakeRepos.json";
        QFile repos(fakePath);
        repos.open(QIODevice::OpenModeFlag::WriteOnly | QIODevice::Text);
        repos.write("[\n]");
        repos.close();
        DataProcessing *dp = DataProcessing::getInstance();
        int expected = 0;

        dp->setFileNameForTests(fakePath);
        dp->removeRepo(31055519);
        repos.open(QIODevice::OpenModeFlag::ReadOnly | QIODevice::Text);
        QByteArray jsonRepos=  repos.readAll() ;
        repos.close();
        QString dirPath = dp->FILE_PATH;
        dp->setFileNameForTests(dirPath.append("/repos.json"));

        QJsonArray readReposArray = QJsonDocument::fromJson(jsonRepos).array();
        int size = readReposArray.size();

        REQUIRE(expected == size);
    }

    SECTION("If file has info about given repo, size will be decreased by 1"){
        QString fakePath = "fakeRepos.json";
        QFile repos(fakePath);
        repos.open(QIODevice::OpenModeFlag::WriteOnly | QIODevice::Text);
        QTextStream stream(&repos);
        stream << "[\n {\n"
                  "\"commitsPerAuthor\": [], "
                  "\"createdAt\": \"2021-11-04T20:02:21\",\n"
                  "            \"description\": \"\",\n"
                  "                    \"groupId\": 14033104,\n"
                  "                    \"groupName\": \"Projects 2021-2022\",\n"
                  "                    \"id\": 31055519,\n"
                  "                    \"lastActivity\": \"2021-12-27T15:44:32\",\n"
                  "                    \"name\": \"MATF / course-rs / Projects 2021-2022 / 13-Git-stat\",\n"
                  "                    \"numOfAuthors\": 11,\n"
                  "                    \"numOfCommits\": 40,\n"
                  "                    \"webUrl\": \"https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2021-2022/13-Git-stat\"\n"
                  "        }\n]";
        repos.close();
        int expected = 0;

        DataProcessing *dc = DataProcessing::getInstance();
        dc->setFileNameForTests(fakePath);
        dc->removeRepo(31055519);
        repos.open(QIODevice::OpenModeFlag::ReadOnly | QIODevice::Text);
        QByteArray jsonRepos =  repos.readAll() ;
        repos.close();
        QString dirPath = dc->FILE_PATH;
        dc->setFileNameForTests(dirPath.append("/repos.json"));
        QJsonArray readReposArray = QJsonDocument::fromJson(jsonRepos).array();
        int size = readReposArray.size();

        REQUIRE(expected == size);
    }
}

TEST_CASE("DataProcessing::checkIfOnDrive","[function]"){

    SECTION("If file is empty, function returns false"){
       QFile repos("fakeRepos1.json");
       repos.open(QIODevice::OpenModeFlag::WriteOnly | QIODevice::Text);
       QTextStream stream(&repos);
       stream << "[\n]";
       DataProcessing *dp = DataProcessing::getInstance();
       QString name("fakeRepos1.json");
       dp->setFileNameForTests(name);

       bool expectedValue = false;
       bool returnValue = dp->checkIfOnDrive(31055519);
       REQUIRE(expectedValue == returnValue);
    }

    SECTION("If file has info about given repo, function returns true"){
       QFile repos("fakeRepos1.json");
       repos.open(QIODevice::OpenModeFlag::WriteOnly | QIODevice::Text);
       QTextStream stream(&repos);
       stream << "[\n {\n"
           "\"commitsPerAuthor\": [], "
           "\"createdAt\": \"2021-11-04T20:02:21\",\n"
           "            \"description\": \"\",\n"
           "                    \"groupId\": 14033104,\n"
           "                    \"groupName\": \"Projects 2021-2022\",\n"
           "                    \"id\": 31055519,\n"
           "                    \"lastActivity\": \"2021-12-27T15:44:32\",\n"
           "                    \"name\": \"MATF / course-rs / Projects 2021-2022 / 13-Git-stat\",\n"
           "                    \"numOfAuthors\": 11,\n"
           "                    \"numOfCommits\": 40,\n"
           "                    \"webUrl\": \"https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2021-2022/13-Git-stat\"\n"
           "        }\n]";
       repos.close();
       DataProcessing *dp = DataProcessing::getInstance();
       QString name("fakeRepos1.json");
       dp->setFileNameForTests(name);

       bool expectedValue = true;

       bool returnValue = dp->checkIfOnDrive(31055519);
       REQUIRE(expectedValue == returnValue);
    }


    SECTION("If file doesn't have info about given repo, function returns false"){
       QFile repos("fakeRepos1.json");
       repos.open(QIODevice::OpenModeFlag::WriteOnly | QIODevice::Text);
       QTextStream stream(&repos);
       stream << "[\n {\n"
           "\"commitsPerAuthor\": [], "
           "\"createdAt\": \"2021-11-04T20:02:21\",\n"
           "            \"description\": \"\",\n"
           "                    \"groupId\": 14033104,\n"
           "                    \"groupName\": \"Projects 2021-2022\",\n"
           "                    \"id\": 31055519,\n"
           "                    \"lastActivity\": \"2021-12-27T15:44:32\",\n"
           "                    \"name\": \"MATF / course-rs / Projects 2021-2022 / 13-Git-stat\",\n"
           "                    \"numOfAuthors\": 11,\n"
           "                    \"numOfCommits\": 40,\n"
           "                    \"webUrl\": \"https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2021-2022/13-Git-stat\"\n"
           "        }\n]";
       repos.close();
       DataProcessing *dp = DataProcessing::getInstance();
       QString name("fakeRepos1.json");
       dp->setFileNameForTests(name);

       bool expectedValue = false;

       bool returnValue = dp->checkIfOnDrive(31340769);
       REQUIRE(expectedValue == returnValue);
    }
}

#endif // DATAPROCESSINGTEST_H
