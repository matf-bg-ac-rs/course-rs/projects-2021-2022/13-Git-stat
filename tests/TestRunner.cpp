//
// Created by luka on 27.12.21..
//

#define CATCH_CONFIG_RUNNER
#include <QCoreApplication>
#include <QTimer>

#include <catch2/catch_session.hpp>

#include "tests/network/NetworkUtilTest.h"
#include "tests/network/GitLabResponseHandlerTest.h"
#include "tests/network/NetworkResponseParserTest.h"
#include "tests/network/SynchronizedLinkedHashMapTest.h"
#include "tests/network/GitLabCommitResponseTest.h"
#include "tests/network/GitLabGroupResponseTest.h"
#include "tests/network/ProjectContexTest.h"
#include "tests/network/GroupContextTest.h"
#include "tests/data/CommentsProcessingTest.h"
#include "tests/data/RepoProcessingTest.h"
#include "tests/data/CommitProcessingTest.h"
#include "tests/data/DataProcessingTest.h"


int main(int argc, char *argv[]) {
    QCoreApplication app(argc, argv);
    QTimer::singleShot(0, [&]{
        app.exit(Catch::Session().run(argc, argv));
    });
    return app.exec();
}

