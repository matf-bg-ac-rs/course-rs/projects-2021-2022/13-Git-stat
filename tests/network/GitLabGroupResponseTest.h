#ifndef GITLABGROUPRESPONSETEST_H
#define GITLABGROUPRESPONSETEST_H

#include <catch2/catch_test_macros.hpp>
#include "include/network/GitLabGroupResponse.h"
#include <QJsonDocument>

TEST_CASE("GitLabGroupResponse::GitLabGroupResponse", "[function]"){
    SECTION("When GitLabGroupResponse::GitLabGroupResponse is called with parameter QJsonObject json, return value is object with attributes from json object"){
        QJsonObject json;
        json.insert("id", 123);
        json.insert("name", "Name");
        json.insert("path", "Path");
        json.insert("description", "Description");
        json.insert("avatar_url", "Url");
        json.insert("full_name","full name");
        json.insert("full_path", "Full path");
        json.insert("created_at", "Mon Nov 15 20:54:23 2021");
        json.insert("parent_id", 1);
        bool expectedValue = true;

        Network::GitLabGroupResponse obj(json);
        bool returnValue = json["id"] == obj.id &&
                           json["name"] == QString::fromStdString(obj.name) &&
                           json["path"] == QString::fromStdString(obj.path) &&
                           json["description"] == QString::fromStdString(obj.description) &&
                           json["avatar_url"] == QString::fromStdString(obj.avatarUrl) &&
                           json["full_name"] == QString::fromStdString(obj.fullName) &&
                           json["full_path"] == QString::fromStdString(obj.fullPath) &&
                           json["created_at"] == QString::fromStdString(obj.createdAt) &&
                           json["parent_id"] == obj.parentId;

        REQUIRE(expectedValue == returnValue);
    }
}

#endif // GITLABGROUPRESPONSETEST_H
