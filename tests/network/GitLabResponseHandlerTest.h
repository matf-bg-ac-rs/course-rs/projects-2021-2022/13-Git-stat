//
// Created by luka on 27.12.21..
//

#ifndef GITSTAT_GITLABRESPONSEHANDLERTEST_H
#define GITSTAT_GITLABRESPONSEHANDLERTEST_H

#include <catch2/catch_test_macros.hpp>
#include "include/network/GitLabResponseHandler.h"
#include "tests/helpers/FakeNetworkRequester.h"
#include "tests/helpers/FakeQNetworkReply.h"
#include <QSignalSpy>

TEST_CASE("GitLabResponseHandler::handleGroupResponse", "[method]") {
    SECTION("When calling method with group data, add it to group context and call NetworkRequester") {
        //Arrange
        const int groupId = 1;
        const QString groupName = "test";

        std::shared_ptr<NetworkContext> networkContext = std::make_shared<NetworkContext>();
        std::shared_ptr<GroupContext> groupContext = std::make_shared<GroupContext>();
        std::shared_ptr<ProjectContext> projectContext = std::make_shared<ProjectContext>();
        groupContext->createOrGet(1);
        FakeNetworkRequester requester(networkContext);
        GitLabResponseHandler responseHandler(nullptr, groupContext, projectContext, networkContext, &requester);
        QJsonObject response;
        response.insert("id", QJsonValue(groupId));
        response.insert("name", groupName);

        //Act
        responseHandler.handleGroupResponse(nullptr, QJsonDocument(response));

        //Assert
        REQUIRE(groupContext->get(1)->name == groupName.toStdString());
        REQUIRE(requester.timesInvoked() == 1);
        REQUIRE(requester.getLastRequestId() == 1);
    }
}

TEST_CASE("GitLabResponseHandler::handleGroupProjectsResponse", "[method]") {

    std::shared_ptr<NetworkContext> networkContext = std::make_shared<NetworkContext>();

    QJsonArray responseData;
    QJsonObject project1;
    project1.insert("id", QJsonValue(10));
    responseData.push_back(project1);
    QJsonObject project2;
    project2.insert("id", QJsonValue(11));
    responseData.push_back(project2);

    SECTION("When calling method with reply without link header and populated group context, add projects to context and emit downloaded signal") {
        //Arrange
        const int groupId = 1;

        std::shared_ptr<GroupContext> groupContext = std::make_shared<GroupContext>();
        std::shared_ptr<ProjectContext> projectContext = std::make_shared<ProjectContext>();
        groupContext->createOrGet(groupId);
        QJsonObject groupJson;
        groupJson.insert("id", 1);
        groupContext->setData(std::make_shared<GitLabGroupResponse>(groupJson));
        FakeNetworkRequester requester(networkContext);
        GitLabResponseHandler responseHandler(nullptr, groupContext, projectContext, networkContext, &requester);

        FakeQNetworkReply reply;
        reply.setProperty(PROP_REQUEST_ID, groupId);

        QSignalSpy signalSpy(&responseHandler, &GitLabResponseHandler::downloadedProjectsInGroup);

        //Act
        responseHandler.handleGroupProjectsResponse(&reply, QJsonDocument(responseData));

        //Assert
        REQUIRE(groupContext->size() == 0);
        REQUIRE(signalSpy.count() == 1);
        REQUIRE(requester.timesInvoked() == 0);
        auto groupResponse = signalSpy.at(0).value(0).value<std::shared_ptr<GitLabGroupResponse>>();
        REQUIRE(groupResponse->id == groupId);
        REQUIRE(groupResponse->projects.size() == 2);
        REQUIRE(groupResponse->projects[0]->id == 10);
        REQUIRE(groupResponse->projects[1]->id == 11);
    }

    SECTION("When calling method with reply with link header containing next ref and populated group context, add projects to context and make request to get next page") {
        //Arrange
        const int groupId = 1;
        const std::string nextUrl = "https://test";

        std::shared_ptr<GroupContext> groupContext = std::make_shared<GroupContext>();
        std::shared_ptr<ProjectContext> projectContext = std::make_shared<ProjectContext>();
        groupContext->createOrGet(groupId);
        QJsonObject groupJson;
        groupJson.insert("id", 1);
        groupContext->setData(std::make_shared<GitLabGroupResponse>(groupJson));
        FakeNetworkRequester requester(networkContext);
        GitLabResponseHandler responseHandler(nullptr, groupContext, projectContext, networkContext, &requester);

        FakeQNetworkReply reply;
        reply.setProperty(PROP_REQUEST_ID, groupId);
        reply.setRawHeader("link", QByteArray::fromStdString("<" + nextUrl + ">; rel=next"));

        QSignalSpy signalSpy(&responseHandler, &GitLabResponseHandler::downloadedProjectsInGroup);

        //Act
        responseHandler.handleGroupProjectsResponse(&reply, QJsonDocument(responseData));

        //Assert
        REQUIRE(groupContext->get(1)->projects.size() == 2);
        REQUIRE(groupContext->get(1)->projects[0]->id == 10);
        REQUIRE(groupContext->get(1)->projects[1]->id == 11);
        REQUIRE(signalSpy.count() == 0);
        REQUIRE(requester.timesInvoked() == 1);
        REQUIRE(requester.getLastUrl() == nextUrl);
        REQUIRE(requester.getLastRequestId() == 1);
    }

    SECTION("When calling method with reply with link header not containing next ref and populated group context, add projects to context and emit downloaded") {
        //Arrange
        const int groupId = 1;
        const std::string nextUrl = "https://test";

        std::shared_ptr<GroupContext> groupContext = std::make_shared<GroupContext>();
        std::shared_ptr<ProjectContext> projectContext = std::make_shared<ProjectContext>();
        groupContext->createOrGet(groupId);
        QJsonObject groupJson;
        groupJson.insert("id", 1);
        groupContext->setData(std::make_shared<GitLabGroupResponse>(groupJson));
        FakeNetworkRequester requester(networkContext);
        GitLabResponseHandler responseHandler(nullptr, groupContext, projectContext, networkContext, &requester);

        FakeQNetworkReply reply;
        reply.setProperty(PROP_REQUEST_ID, groupId);
        reply.setRawHeader("link", QByteArray::fromStdString("<" + nextUrl + ">; rel=prev"));

        QSignalSpy signalSpy(&responseHandler, &GitLabResponseHandler::downloadedProjectsInGroup);

        //Act
        responseHandler.handleGroupProjectsResponse(&reply, QJsonDocument(responseData));

        //Assert
        REQUIRE(groupContext->size() == 0);
        REQUIRE(signalSpy.count() == 1);
        REQUIRE(requester.timesInvoked() == 0);
        auto groupResponse = signalSpy.at(0).value(0).value<std::shared_ptr<GitLabGroupResponse>>();
        REQUIRE(groupResponse->id == groupId);
        REQUIRE(groupResponse->projects.size() == 2);
        REQUIRE(groupResponse->projects[0]->id == 10);
        REQUIRE(groupResponse->projects[1]->id == 11);
    }
}

TEST_CASE("GitLabResponseHandler::handleProjectResponse", "[method]") {
    SECTION("When calling method with project response, emit downloadedProjectInfo response") {
        //Arrange
        int projectId = 1;

        std::shared_ptr<NetworkContext> networkContext = std::make_shared<NetworkContext>();
        std::shared_ptr<GroupContext> groupContext = std::make_shared<GroupContext>();
        std::shared_ptr<ProjectContext> projectContext = std::make_shared<ProjectContext>();
        FakeNetworkRequester requester(networkContext);
        GitLabResponseHandler responseHandler(nullptr, groupContext, projectContext, networkContext, &requester);
        QJsonObject response;
        response.insert("id", QJsonValue(projectId));

        QSignalSpy signalSpy(&responseHandler, &GitLabResponseHandler::downloadedProjectInfo);

        //Act
        responseHandler.handleProjectResponse(nullptr, QJsonDocument(response));

        //Assert
        REQUIRE(signalSpy.count() == 1);
        auto project = signalSpy.at(0).value(0).value<std::shared_ptr<GitLabProjectResponse>>();
        REQUIRE(project->id == projectId);
    }
}

TEST_CASE("GitLabResponseHandler::handleCommitsListResponse", "[method]") {
    QJsonArray response;
    QJsonObject commit1;
    commit1.insert("id", "aaaa");
    response.push_back(commit1);
    QJsonObject commit2;
    commit2.insert("id", "bbbb");
    response.push_back(commit2);

    SECTION("When calling method with project context without requested project, fire error") {
        //Arrange
        const int projectIdInContext = 1;
        const int projectIdInRequest = 2;

        std::shared_ptr<NetworkContext> networkContext = std::make_shared<NetworkContext>();
        std::shared_ptr<GroupContext> groupContext = std::make_shared<GroupContext>();
        std::shared_ptr<ProjectContext> projectContext = std::make_shared<ProjectContext>();
        projectContext->createOrGet(projectIdInContext);
        FakeNetworkRequester requester(networkContext);
        GitLabResponseHandler responseHandler(nullptr, groupContext, projectContext, networkContext, &requester);
        FakeQNetworkReply networkReply;
        networkReply.setProperty(PROP_REQUEST_ID, projectIdInRequest);

        QSignalSpy signalSpy(&responseHandler, &GitLabResponseHandler::fireError);

        //Act
        responseHandler.handleCommitsListResponse(&networkReply, QJsonDocument(response));

        //Assert
        REQUIRE(signalSpy.count() == 1);
        int requestId = signalSpy.at(0).value(1).value<int>();
        REQUIRE(projectIdInRequest == requestId);
        REQUIRE(requester.timesInvoked() == 0);
    }

    SECTION("When calling method with populated project context and response with link header, follow next link and start downloading commit details") {
        //Arrange
        const int projectId = 1;

        std::shared_ptr<NetworkContext> networkContext = std::make_shared<NetworkContext>();
        std::shared_ptr<GroupContext> groupContext = std::make_shared<GroupContext>();
        std::shared_ptr<ProjectContext> projectContext = std::make_shared<ProjectContext>();
        projectContext->createOrGet(projectId);
        FakeNetworkRequester requester(networkContext);
        GitLabResponseHandler responseHandler(nullptr, groupContext, projectContext, networkContext, &requester);
        FakeQNetworkReply networkReply;
        networkReply.setProperty(PROP_REQUEST_ID, projectId);
        networkReply.setRawHeader("link", "<https://test>; rel=next");

        //Act
        responseHandler.handleCommitsListResponse(&networkReply, QJsonDocument(response));

        //Assert
        REQUIRE(projectContext->getTotalCommitsCount(projectId) == 2);
        REQUIRE(requester.timesInvoked() == 3);
        REQUIRE(requester.getLastRequestId() == projectId);
        REQUIRE(requester.getLastCommitId() == "bbbb");
    }

    SECTION("When calling method with populated project context and response without link header, finalize commit count and start downloading commit details") {
        //Arrange
        const int projectId = 1;

        std::shared_ptr<NetworkContext> networkContext = std::make_shared<NetworkContext>();
        std::shared_ptr<GroupContext> groupContext = std::make_shared<GroupContext>();
        std::shared_ptr<ProjectContext> projectContext = std::make_shared<ProjectContext>();
        projectContext->createOrGet(projectId);
        FakeNetworkRequester requester(networkContext);
        GitLabResponseHandler responseHandler(nullptr, groupContext, projectContext, networkContext, &requester);
        FakeQNetworkReply networkReply;
        networkReply.setProperty(PROP_REQUEST_ID, projectId);

        //Act
        responseHandler.handleCommitsListResponse(&networkReply, QJsonDocument(response));

        //Assert
        REQUIRE(requester.timesInvoked() == 2);
        REQUIRE(requester.getLastRequestId() == projectId);
        REQUIRE(requester.getLastCommitId() == "bbbb");
        REQUIRE(projectContext->isFinishedCommitCount(projectId));
        REQUIRE(projectContext->getTotalCommitsCount(projectId) == 2);
    }
}

TEST_CASE("GitLabResponseHandler::handleCommitsResponse", "[method]") {
    SECTION("When calling method with commit response, insert it to context and make a call to retrieve diff") {
        //Arrange
        const int projectId = 1;

        std::shared_ptr<NetworkContext> networkContext = std::make_shared<NetworkContext>();
        std::shared_ptr<GroupContext> groupContext = std::make_shared<GroupContext>();
        std::shared_ptr<ProjectContext> projectContext = std::make_shared<ProjectContext>();
        projectContext->createOrGet(projectId);
        FakeNetworkRequester requester(networkContext);
        GitLabResponseHandler responseHandler(nullptr, groupContext, projectContext, networkContext, &requester);
        FakeQNetworkReply networkReply;
        networkReply.setProperty(PROP_REQUEST_ID, projectId);
        QJsonObject response;
        response.insert("id", "aaaa");

        //Act
        responseHandler.handleCommitResponse(&networkReply, QJsonDocument(response));

        //Assert
        REQUIRE(requester.timesInvoked() == 1);
        REQUIRE(requester.getLastRequestId() == projectId);
        REQUIRE(requester.getLastCommitId() == "aaaa");
        REQUIRE(requester.getLastUrl().ends_with("diff"));
        REQUIRE(projectContext->getCommits(projectId)->size() == 1);
        REQUIRE(projectContext->getCommits(projectId)->front()->id == "aaaa");
    }
}

TEST_CASE("GitLabResponseHandler::handleCommitDiffResponse", "[method]") {
    QJsonArray response;
    QJsonObject diff1;
    diff1.insert("old_path", "qqqq");
    response.push_back(diff1);
    QJsonObject diff2;
    diff2.insert("old_path", "wwww");
    response.push_back(diff2);

    SECTION("When calling method with project context without requested project, fire error") {
        //Arrange
        const int projectIdInContext = 1;
        const int projectIdInRequest = 2;

        std::shared_ptr<NetworkContext> networkContext = std::make_shared<NetworkContext>();
        std::shared_ptr<GroupContext> groupContext = std::make_shared<GroupContext>();
        std::shared_ptr<ProjectContext> projectContext = std::make_shared<ProjectContext>();
        projectContext->createOrGet(projectIdInContext);
        FakeNetworkRequester requester(networkContext);
        GitLabResponseHandler responseHandler(nullptr, groupContext, projectContext, networkContext, &requester);
        FakeQNetworkReply networkReply;
        networkReply.setProperty(PROP_REQUEST_ID, projectIdInRequest);

        QSignalSpy signalSpy(&responseHandler, &GitLabResponseHandler::fireError);

        //Act
        responseHandler.handleCommitDiffResponse(&networkReply, QJsonDocument(response));

        //Assert
        REQUIRE(signalSpy.count() == 1);
        int requestId = signalSpy.at(0).value(1).value<int>();
        REQUIRE(projectIdInRequest == requestId);
        REQUIRE(requester.timesInvoked() == 0);
    }

    SECTION("When calling method with populated project context and non-final commits, insert diffs and increment commit count") {
        //Arrange
        const int projectId = 1;

        std::shared_ptr<NetworkContext> networkContext = std::make_shared<NetworkContext>();
        std::shared_ptr<GroupContext> groupContext = std::make_shared<GroupContext>();
        std::shared_ptr<ProjectContext> projectContext = std::make_shared<ProjectContext>();
        projectContext->createOrGet(projectId);
        projectContext->addCommitCount(projectId, 50);
        QJsonObject commitJson;
        commitJson.insert("id", "aaaa");
        projectContext->insertCommits(projectId, std::make_shared<GitLabCommitResponse>(commitJson));
        FakeNetworkRequester requester(networkContext);
        GitLabResponseHandler responseHandler(nullptr, groupContext, projectContext, networkContext, &requester);
        FakeQNetworkReply networkReply;
        networkReply.setProperty(PROP_REQUEST_ID, projectId);
        networkReply.setProperty(PROP_COMMIT_ID, "aaaa");

        //Act
        responseHandler.handleCommitDiffResponse(&networkReply, QJsonDocument(response));

        //Assert
        REQUIRE(requester.timesInvoked() == 0);
        REQUIRE(projectContext->getDownloadedCommitsCount(projectId) == 1);
        auto commit = projectContext->getCommits(projectId)->front();
        REQUIRE(commit->fileChanges.size() == 2);
        REQUIRE(commit->fileChanges[0]->oldFilename == "qqqq");
        REQUIRE(commit->fileChanges[1]->oldFilename == "wwww");
    }

    SECTION("When calling method with populated project context and final commit, finalize commit download") {
        //Arrange
        const int projectId = 1;

        std::shared_ptr<NetworkContext> networkContext = std::make_shared<NetworkContext>();
        std::shared_ptr<GroupContext> groupContext = std::make_shared<GroupContext>();
        std::shared_ptr<ProjectContext> projectContext = std::make_shared<ProjectContext>();
        projectContext->createOrGet(projectId);
        projectContext->addCommitCount(projectId, 1);
        projectContext->finishCommitCount(projectId);
        QJsonObject commitJson;
        commitJson.insert("id", "aaaa");
        projectContext->insertCommits(projectId, std::make_shared<GitLabCommitResponse>(commitJson));
        FakeNetworkRequester requester(networkContext);
        GitLabResponseHandler responseHandler(nullptr, groupContext, projectContext, networkContext, &requester);
        FakeQNetworkReply networkReply;
        networkReply.setProperty(PROP_REQUEST_ID, projectId);
        networkReply.setProperty(PROP_COMMIT_ID, "aaaa");

        QSignalSpy signalSpy(&responseHandler, &GitLabResponseHandler::downloadedCommitInfo);

        //Act
        responseHandler.handleCommitDiffResponse(&networkReply, QJsonDocument(response));

        //Assert
        REQUIRE(signalSpy.count() == 1);
        REQUIRE(projectContext->size() == 0);
        auto projectIdInSignal = signalSpy.at(0).value(0).value<int>();
        auto commitList = signalSpy.at(0).value(1).value<std::shared_ptr<std::list<std::shared_ptr<GitLabCommitResponse>>>>();
        REQUIRE(projectIdInSignal == projectId);
        REQUIRE(commitList->size() == 1);
        REQUIRE(commitList->front()->id == "aaaa");
    }
}

#endif //GITSTAT_GITLABRESPONSEHANDLERTEST_H
