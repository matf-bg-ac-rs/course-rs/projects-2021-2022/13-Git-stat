#ifndef GITLABCOMMITRESPONSETEST_H
#define GITLABCOMMITRESPONSETEST_H

#include <catch2/catch_test_macros.hpp>
#include "include/network/GitLabCommitResponse.h"
#include <QJsonDocument>


TEST_CASE("GitLabCommitResponse::GitLabCommitResponse", "[function]"){
    SECTION("When GitLabCommitResponse::GitLabCommitResponse is called with parameter QJsonObject json, return value is object with attributes from json object"){
        QJsonObject json;
        json.insert("id", "123");
        json.insert("short_id", "100");
        json.insert("created_at", "Mon Nov 15 20:54:23 2021");
        QJsonArray array;
        array.push_back("1");
        array.push_back("2");
        array.push_back("3");
        json.insert("parent_ids", array);
        json.insert("title", "Git-stat");
        json.insert("message", "Fixed");
        json.insert("author_name", "Mira");
        json.insert("author_email", "mira@gmail.com");
        json.insert("authored_date", "Mon Nov 15 20:54:23 2021");
        json.insert("committer_name", "Mira");
        json.insert("committer_email", "mira@gmail.com");
        json.insert("committed_date", "Mon Nov 15 20:54:23 2021");
        json.insert("web_url", "neki url");
        QJsonObject stats;
        stats.insert("additions", 12);
        stats.insert("deletions", 1);
        stats.insert("total", 13);
        json.insert("stats", stats);
        bool expectedValue = true;

        Network::GitLabCommitResponse obj(json);
        bool returnValue = json["id"].toString() == QString::fromStdString(obj.id) &&
                           json["short_id"].toString() == QString::fromStdString(obj.shortId) &&
                           json["created_at"].toString() == QString::fromStdString(obj.createdAt) &&
                           json["parent_ids"].toArray().at(0).toString() == QString::fromStdString(obj.parentIds.at(0)) &&
                           json["parent_ids"].toArray().at(1).toString() == QString::fromStdString(obj.parentIds.at(1)) &&
                           json["parent_ids"].toArray().at(2).toString() == QString::fromStdString(obj.parentIds.at(2)) &&
                           json["title"].toString() == QString::fromStdString(obj.title) &&
                           json["message"].toString() == QString::fromStdString(obj.message) &&
                           json["author_name"].toString() == QString::fromStdString(obj.authorName) &&
                           json["author_email"].toString() == QString::fromStdString(obj.authorEmail) &&
                           json["authored_date"].toString() == QString::fromStdString(obj.authoredDate) &&
                           json["committer_name"].toString() == QString::fromStdString(obj.committerName) &&
                           json["committer_email"].toString() == QString::fromStdString(obj.committerEmail) &&
                           json["committed_date"].toString() == QString::fromStdString(obj.committedDate) &&
                           json["web_url"].toString() == QString::fromStdString(obj.webUrl) &&
                           json["stats"].toObject().value("additions") == obj.stats.additions &&
                           json["stats"].toObject().value("deletions") == obj.stats.deletions &&
                           json["stats"].toObject().value("total") == obj.stats.total;

        REQUIRE(returnValue == expectedValue);
    }

        SECTION("When GitLabCommitResponse::GitLabCommitResponse is called with parameter QJsonObject json with empty value in field stat, return value is object with attributes from json object"){
            QJsonObject json;
            json.insert("id", "123");
            json.insert("short_id", "100");
            json.insert("created_at", "Mon Nov 15 20:54:23 2021");
            QJsonArray array;
            array.push_back("1");
            array.push_back("2");
            array.push_back("3");
            json.insert("parent_ids", array);
            json.insert("title", "Git-stat");
            json.insert("message", "Fixed");
            json.insert("author_name", "Mira");
            json.insert("author_email", "mira@gmail.com");
            json.insert("authored_date", "Mon Nov 15 20:54:23 2021");
            json.insert("committer_name", "Mira");
            json.insert("committer_email", "mira@gmail.com");
            json.insert("committed_date", "Mon Nov 15 20:54:23 2021");
            json.insert("web_url", "neki url");
            QJsonObject stats;
            stats.insert("additions", "");
            stats.insert("deletions", "");
            stats.insert("total", "");
            json.insert("stats", stats);
            bool expectedValue = true;

            Network::GitLabCommitResponse obj(json);
            bool returnValue = json["id"].toString() == QString::fromStdString(obj.id) &&
                               json["short_id"].toString() == QString::fromStdString(obj.shortId) &&
                               json["created_at"].toString() == QString::fromStdString(obj.createdAt) &&
                               json["parent_ids"].toArray().at(0).toString() == QString::fromStdString(obj.parentIds.at(0)) &&
                               json["parent_ids"].toArray().at(1).toString() == QString::fromStdString(obj.parentIds.at(1)) &&
                               json["parent_ids"].toArray().at(2).toString() == QString::fromStdString(obj.parentIds.at(2)) &&
                               json["title"].toString() == QString::fromStdString(obj.title) &&
                               json["message"].toString() == QString::fromStdString(obj.message) &&
                               json["author_name"].toString() == QString::fromStdString(obj.authorName) &&
                               json["author_email"].toString() == QString::fromStdString(obj.authorEmail) &&
                               json["authored_date"].toString() == QString::fromStdString(obj.authoredDate) &&
                               json["committer_name"].toString() == QString::fromStdString(obj.committerName) &&
                               json["committer_email"].toString() == QString::fromStdString(obj.committerEmail) &&
                               json["committed_date"].toString() == QString::fromStdString(obj.committedDate) &&
                               json["web_url"].toString() == QString::fromStdString(obj.webUrl) &&
                               json["stats"].toObject().value("additions").toInt() == obj.stats.additions &&
                               json["stats"].toObject().value("deletions").toInt() == obj.stats.deletions &&
                               json["stats"].toObject().value("total").toInt() == obj.stats.total;

            REQUIRE(returnValue == expectedValue);
        }


}


TEST_CASE("GitLabCommitResponse::FileChange", "[function]"){
    SECTION("When GitLabCommitResponse::FileChange is called with parameter QJsonObject json, return value is object with attributes from json object"){
        QJsonObject json;
        json.insert("old_path", "stari naziv");
        json.insert("new_path", "Novi naziv");
        json.insert("new_file", "true");
        json.insert("renamed_file", "false");
        json.insert("deleted_file", "false");
        bool expectedValue = true;

        Network::GitLabCommitResponse::FileChange obj(json);
        bool returnValue = json["old_path"].toString() == QString::fromStdString(obj.oldFilename) &&
                           json["new_path"].toString() == QString::fromStdString(obj.newFilename) &&
                           json["new_file"].toBool() == obj.isFileCreated &&
                           json["renamed_file"].toBool() == obj.isFileRenamed &&
                           json["deleted_file"].toBool() == obj.isFileDeleted;

        REQUIRE(returnValue == expectedValue);

    }
}

#endif // GITLABCOMMITRESPONSETEST_H
