#ifndef GROUPCONTEXTTEST_H
#define GROUPCONTEXTTEST_H

#include <catch2/catch_test_macros.hpp>
#include "include/network/GroupContext.h"

TEST_CASE("GroupContext::createOrGet", "[function]"){
    SECTION("When one element is inserted in GroupContext::contextMap, return value of createOrGet method is not nullptr"){
        Network::GroupContext group_context;
        int groupId = 1;
        auto expectedValue = nullptr;

        group_context.createOrGet(groupId);
        auto returnValue = group_context.createOrGet(groupId);

        REQUIRE_FALSE(returnValue == expectedValue);
    }

    SECTION("When existing key is erased from GroupContext::contextMap, return value of createOrGet, with parameter groupId of erased element, method is nullptr"){
        Network::GroupContext group_context;
        int groupId = 1;
        auto expectedValue = nullptr;

        group_context.createOrGet(groupId);
        group_context.erase(groupId);
        auto returnValue = group_context.createOrGet(groupId);

        REQUIRE(returnValue == expectedValue);
    }

    SECTION("When existing key is softErased from GroupContext::contextMap, return value of createOrGet, with parameter groupId of erased element, method is nullptr"){
        Network::GroupContext group_context;
        int groupId = 1;
        auto expectedValue = nullptr;

        group_context.createOrGet(groupId);
        group_context.softErase(groupId);
        auto returnValue = group_context.createOrGet(groupId);

        REQUIRE(returnValue == expectedValue);
    }
}


TEST_CASE("GroupContext::erase", "[function]"){
    SECTION("When one element with groupId key is erased from GroupContext::contextMap, size of map is decremented by one"){
        Network::GroupContext group_context;
        int groupId = 1;
        unsigned long expectedValue = 0;

        group_context.createOrGet(groupId);
        group_context.erase(groupId);
        unsigned long returnValue = group_context.size();

        REQUIRE(expectedValue == returnValue);
    }

    SECTION("When one element with groupId key is softErased from GroupContext::contextMap, size of map is not decremented"){
        Network::GroupContext group_context;
        int groupId = 1;
        unsigned long expectedValue = 1;

        group_context.createOrGet(groupId);
        group_context.softErase(groupId);
        unsigned long returnValue = group_context.size();

        REQUIRE(expectedValue == returnValue);
    }
}


TEST_CASE("GroupContext::get", "[function]"){
    SECTION("When non-existing element is taken from GroupContext::contextMap, return value od get method is nullptr"){
        Network::GroupContext group_context;
        int groupId = 1;
        int non_existing_id = 3;
        auto expectedValue = nullptr;

        group_context.createOrGet(groupId);
        auto returnValue = group_context.get(non_existing_id);

        REQUIRE(returnValue == expectedValue);

    }


    SECTION("When existing element is taken from GroupContext::contextMap, return value of get method is not nullptr"){
        Network::GroupContext group_context;
        int groupId = 1;
        QJsonObject json;
        std::shared_ptr<Network::GitLabGroupResponse> groupInfo = std::make_shared<Network::GitLabGroupResponse>(json);
        groupInfo->id = groupId;
        auto expectedValue = nullptr;

        group_context.createOrGet(groupId);
        group_context.setData(groupInfo);
        auto returnValue = group_context.get(groupId);

        REQUIRE_FALSE(returnValue == expectedValue);

    }


    SECTION("When existing element is softErased from GroupContext::contextMap, return value of get method is not nullptr"){
        Network::GroupContext group_context;
        int groupId = 1;
        QJsonObject json;
        std::shared_ptr<Network::GitLabGroupResponse> groupInfo = std::make_shared<Network::GitLabGroupResponse>(json);
        groupInfo->id = groupId;
        auto expectedValue = nullptr;

        group_context.createOrGet(groupId);
        group_context.setData(groupInfo);
        group_context.softErase(groupId);
        auto returnValue = group_context.get(groupId);

        REQUIRE_FALSE(returnValue == expectedValue);

    }

}


TEST_CASE("GroupContext::insertProject", "[function]"){
    SECTION("When a project is inserted in GroupContext::groupInfo->projects, return value of get method is not nullptr"){
        Network::GroupContext group_context;
        int groupId = 1;
        QJsonObject json;
        std::shared_ptr<Network::GitLabProjectResponse> project = std::make_shared<Network::GitLabProjectResponse>(json);
        std::shared_ptr<Network::GitLabGroupResponse> groupInfo = std::make_shared<Network::GitLabGroupResponse>(json);
        groupInfo->id = groupId;
        auto expectedValue = nullptr;

        group_context.createOrGet(groupId);
        group_context.setData(groupInfo);
        group_context.insertProject(groupId, project);
        auto returnValue = group_context.get(groupId);

        REQUIRE_FALSE(returnValue == expectedValue);
    }
}


#endif // GROUPCONTEXTTEST_H





