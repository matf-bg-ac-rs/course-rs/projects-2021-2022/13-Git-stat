#ifndef PROJECTCONTEXTEST_H
#define PROJECTCONTEXTEST_H

#include <catch2/catch_test_macros.hpp>
#include "include/network/ProjectContext.h"



TEST_CASE("ProjectContext::erase", "[function]"){

    SECTION("When one element with projectId key is erased from ProjectContext::contextMap, size of map is decremented by one"){
        Network::ProjectContext project_context;
        int projectId = 1;
        unsigned long expectedValue = 0;

        project_context.createOrGet(projectId);
        project_context.erase(projectId);
        unsigned long returnValue = project_context.size();

        REQUIRE(returnValue == expectedValue);

    }

    SECTION("When one element with projectId key is SoftErased from ProjectContext::contextMap, size of map is not decremented"){
        Network::ProjectContext project_context;
        int projectId = 1;
        unsigned long expectedValue = 1;

        project_context.createOrGet(projectId);
        project_context.softErase(projectId);
        unsigned long returnValue = project_context.size();

        REQUIRE(returnValue == expectedValue);

    }


}


TEST_CASE("ProjectContext::createOrGet", "[function]"){
    SECTION("When one projectId is added to ProjectContext::contextMap, retunValue of createOrGet method with parameter projectId is not nullptr"){
        Network::ProjectContext project_context;
        int projectId = 1;
        auto expectedValue = nullptr;

        project_context.createOrGet(projectId);
        auto returnValue = project_context.createOrGet(projectId);

        REQUIRE_FALSE(returnValue == expectedValue);
    }

    SECTION("When existing key is erased from ProjectContext::contextMap, return value of createOrGet, with parameter projectId of erased element, method is nullptr"){
        Network::ProjectContext project_context;
        int projectId = 1;
        auto expectedValue = nullptr;

        project_context.createOrGet(projectId);
        project_context.erase(projectId);
        auto returnValue = project_context.createOrGet(projectId);

        REQUIRE(returnValue == expectedValue);
    }



    SECTION("When existing key is softErased from ProjectContext::contextMap, return value of createOrGet ,with parameter projectId of softErased element, method is nullptr"){
        Network::ProjectContext project_context;
        int projectId = 1;
        auto expectedValue = nullptr;

        project_context.createOrGet(projectId);
        project_context.softErase(projectId);
        auto returnValue = project_context.createOrGet(projectId);

        REQUIRE(returnValue == expectedValue);
    }

}


TEST_CASE("ProjectContext::empty", "[function]"){
    SECTION("When existing key is softErased from ProjectContext::contextMap with one element, return value of isEmpty method is true"){
        Network::ProjectContext project_context;
        int projectId = 1;
        bool expectedValue = true;

        project_context.createOrGet(projectId);
        project_context.softErase(projectId);
        bool returnValue = project_context.isEmpty();

        REQUIRE(expectedValue == returnValue);

    }

    SECTION("When existing key is erased from ProjectContext::contextMap with one element, return value of isEmpty method is true"){
        Network::ProjectContext project_context;
        int projectId = 1;
        bool expectedValue = true;

        project_context.createOrGet(projectId);
        project_context.erase(projectId);
        bool returnValue = project_context.isEmpty();

        REQUIRE(expectedValue == returnValue);

    }
}


TEST_CASE("ProjectContext::exists", "[function]"){
    SECTION("When existing key is erased from ProjectContext::contextMap with one element, return value of exists method is flase"){
        Network::ProjectContext project_context;
        int projectId = 1;
        bool expectedValue = false;

        project_context.createOrGet(projectId);
        project_context.erase(projectId);
        bool returnValue = project_context.exists(projectId);

        REQUIRE(expectedValue == returnValue);

    }


    SECTION("When existing key is softErased from ProjectContext::contextMap with one element, return value of exists method is true"){
        Network::ProjectContext project_context;
        int projectId = 1;
        bool expectedValue = true;

        project_context.createOrGet(projectId);
        project_context.softErase(projectId);
        bool returnValue = project_context.exists(projectId);

        REQUIRE(expectedValue == returnValue);

    }
}


TEST_CASE("ProjectContext::getCommit", "[function]"){

    SECTION("When existing element is taken from ProjectContext::contextMap, return value od get method is nullptr"){
        Network::ProjectContext project_context;
        int projectId = 1;
        int non_existed_id = 2;
        project_context.createOrGet(projectId);
        auto expectedValue = nullptr;

        auto returnValue = project_context.get(non_existed_id);

        REQUIRE(returnValue == expectedValue);

    }

    SECTION("When existing element is taken from commits, return value of getCommit method is not nullptr"){
        Network::ProjectContext project_context;
        int projectId = 1;
        int commitId = 2;
        auto expectedValue = nullptr;

        project_context.createOrGet(projectId);
        QJsonObject json;
        std::shared_ptr<Network::GitLabCommitResponse> commit = std::make_shared<Network::GitLabCommitResponse>(json);
        commit->id = commitId;
        project_context.insertCommits(projectId, commit);
        auto returnValue = project_context.getCommit(projectId, commit->id);

        REQUIRE_FALSE(expectedValue == returnValue);
    }


    SECTION("When non-existing element is taken from commits, return value of getCommit method is nullptr"){
        Network::ProjectContext project_context;
        int projectId = 1;
        int commitId = 2;
        int non_exiesting_key = 5;
        auto expectedValue = nullptr;

        project_context.createOrGet(projectId);
        QJsonObject json;
        std::shared_ptr<Network::GitLabCommitResponse> commit = std::make_shared<Network::GitLabCommitResponse>(json);
        commit->id = commitId;
        project_context.insertCommits(projectId, commit);
        auto returnValue = project_context.getCommit(projectId, std::to_string(non_exiesting_key));

        REQUIRE(expectedValue == returnValue);
    }

}


TEST_CASE("ProjectContext::addCommitCount", "[function]"){
    SECTION("When attribute finishedCommitCount from ProjectContext is false, return value of addCommitCount method is true"){
        Network::ProjectContext project_context;
        int projectId = 1;
        int commitCount = 10;
        bool expectedValue = true;

        project_context.createOrGet(projectId);
        bool returnValue = project_context.addCommitCount(projectId, commitCount);

        REQUIRE(expectedValue == returnValue);

    }

    SECTION("When attribute finishedCommitCount from ProjectContext is true, return value of addCommitCount method is false"){
        Network::ProjectContext project_context;
        int projectId = 1;
        int commitCount = 10;
        bool expectedValue = false;

        project_context.createOrGet(projectId);
        project_context.finishCommitCount(projectId);
        bool returnValue = project_context.addCommitCount(projectId, commitCount);

        REQUIRE(expectedValue == returnValue);

    }


}




#endif // PROJECTCONTEXTEST_H
