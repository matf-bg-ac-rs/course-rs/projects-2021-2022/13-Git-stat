//
// Created by luka on 27.12.21..
//

#ifndef GITSTAT_FAKENETWORKREQUESTER_H
#define GITSTAT_FAKENETWORKREQUESTER_H

#include "include/network/NetworkUtils.h"
#include "include/network/NetworkRequester.h"

class FakeNetworkRequester : public Network::NetworkRequester {

public:
    FakeNetworkRequester(const std::shared_ptr<NetworkContext> &networkContext);

    void doGetRequest(const std::string& urlString, const ResponseHandlerFunc& responseHandlerFunc, int requestId,
                      const std::string& commitId) override;

    [[nodiscard]] int timesInvoked() const;
    [[nodiscard]] std::string getLastUrl() const;
    [[nodiscard]] int getLastRequestId() const;
    [[nodiscard]] std::string getLastCommitId() const;

private:
    std::string lastUrl;
    int lastRequestId;
    std::string lastCommitId;
    int invoked = 0;
};


#endif //GITSTAT_FAKENETWORKREQUESTER_H
