//
// Created by luka on 27.12.21..
//

#include "FakeNetworkRequester.h"

void FakeNetworkRequester::doGetRequest(const std::string& urlString, const ResponseHandlerFunc& responseHandlerFunc, int requestId,
                                        const std::string& commitId) {
    invoked++;
    this->lastUrl = urlString;
    this->lastRequestId = requestId;
    this->lastCommitId = commitId;
}

int FakeNetworkRequester::timesInvoked() const {
    return invoked;
}

std::string FakeNetworkRequester::getLastUrl() const {
    return lastUrl;
}

int FakeNetworkRequester::getLastRequestId() const {
    return lastRequestId;
}

std::string FakeNetworkRequester::getLastCommitId() const {
    return lastCommitId;
}

FakeNetworkRequester::FakeNetworkRequester(const std::shared_ptr<NetworkContext> &networkContext)
        : NetworkRequester(nullptr, networkContext, nullptr, nullptr) {}
