//
// Created by luka on 27.12.21..
//

#include "FakeQNetworkReply.h"

#include <QNetworkAccessManager>
#include <QBuffer>
#include <QTimer>

FakeQNetworkReply::FakeQNetworkReply(QObject *parent) : QNetworkReply(parent) {
}

void FakeQNetworkReply::setHttpStatusCode( int code, const QByteArray &statusText ) {
    setAttribute(QNetworkRequest::HttpStatusCodeAttribute, code);

    if (statusText.isNull()) return;
    setAttribute(QNetworkRequest::HttpReasonPhraseAttribute, statusText);
}

void FakeQNetworkReply::setHeader(QNetworkRequest::KnownHeaders header, const QVariant &value) {
    QNetworkReply::setHeader(header, value);
}

void FakeQNetworkReply::setError(QNetworkReply::NetworkError errorCode, const QString &errorString) {
    QNetworkReply::setError(errorCode, errorString);
}

void FakeQNetworkReply::setContentType(const QByteArray &contentType) {
    setHeader(QNetworkRequest::ContentTypeHeader, contentType);
}

void FakeQNetworkReply::setContent(const QString &content) {
    open(ReadOnly | Unbuffered);
    setHeader(QNetworkRequest::ContentLengthHeader, QVariant(content.size()));

    QTimer::singleShot(0, this, &FakeQNetworkReply::readyRead);
    QTimer::singleShot( 0, this, &FakeQNetworkReply::finished);
}

void FakeQNetworkReply::abort() {
}


qint64 FakeQNetworkReply::bytesAvailable() const {
    return 0;
}

bool FakeQNetworkReply::isSequential() const {
    return true;
}

qint64 FakeQNetworkReply::readData(char *data, qint64 maxSize) {
    return -1;
}

void FakeQNetworkReply::setRawHeader(const QByteArray& name, const QByteArray& value) {
    QNetworkReply::setRawHeader(name, value);
}

void FakeQNetworkReply::setUrl(const QUrl &url) {
    QNetworkReply::setUrl(url);
}

