//
// Created by luka on 26.12.21..
//

// TESTS:
// 2-3 tests: void populateRateLimitDataFromReply(QNetworkReply* reply) - sa / bez nekog od headera, svih headera

#ifndef GITSTAT_NETWORKCONTEXT_H
#define GITSTAT_NETWORKCONTEXT_H

#include <cstdint>
#include <unordered_set>
#include <string>
#include <QNetworkReply>

namespace Network {
    class NetworkContext;
}

using namespace Network;

class Network::NetworkContext {
public:
    int rateLimitRemaining = 999999; //this is above and beyond the actual rate limit (usually 500 or 2000)
    uint64_t rateLimitResetsAt = 0; //unix timestamp
    uint64_t lastErrorFiredAt = 0;
    uint64_t lastSleepTime = 0;

    bool authRequestInProgress = false;
    std::string authToken;

    std::unordered_set<std::string> retriedRequests;

    void populateRateLimitDataFromReply(QNetworkReply* reply);
};


#endif //GITSTAT_NETWORKCONTEXT_H
