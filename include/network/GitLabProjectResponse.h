//
// Created by luka on 22.11.21..
//

// TESTS:
// ~2 tests: GitLabProjectResponse(QJsonObject& json) - Parsiranje JSONa u objekat

#ifndef GITSTAT_GITLABPROJECTRESPONSE_H
#define GITSTAT_GITLABPROJECTRESPONSE_H

#include<string>
#include<vector>
#include <QJsonObject>
#include <QJsonValue>
#include <QJsonArray>
#include <ostream>
#include "NetworkUtils.h"

namespace Network {
    class GitLabProjectResponse;
}

class Network::GitLabProjectResponse {
public:
    int id;
    std::string description;
    std::string name;
    std::string nameWithNamespace;
    std::string path;
    std::string pathWithNamespace;
    std::string createdAt;
    std::string defaultBranch;
    std::vector<std::string> tagList;
    std::vector<std::string> topics;
    std::string sshUrlToRepo;
    std::string httpUrlToRepo;
    std::string webUrl;
    std::string avatarUrl;
    std::string groupName;
    int groupId;
    int forksCount;
    int starCount;
    std::string lastActivityAt;

    explicit GitLabProjectResponse(QJsonObject& json);

    operator QString() const;

private:
    const static inline QString GROUP_KIND_PROJECT= QString("group");
};


#endif //GITSTAT_GITLABPROJECTRESPONSE_H
