//
// Created by luka on 26.12.21..
//

#ifndef GITSTAT_NETWORKRESPONSEPARSER_H
#define GITSTAT_NETWORKRESPONSEPARSER_H

#include <memory>
#include "NetworkContext.h"
#include "NetworkRequester.h"
#include "NetworkUtils.h"

// TESTS:
// ~7 tests: parseNetworkError - no error, rate limit error (with / without Retry-After, before / after rate limit is
//                               reset), retriable / non-retriable error
// ~5 tests: retryAfter - null/non-null reply, force sleep, stale request, consecutive sleeps; check if doGetRequest is called
// (??) parseResponseSlot
// =====================
// TOTAL: ~12

namespace Network {
    class NetworkResponseParser;
    class NetworkRequester;
}

using namespace Network;

class Network::NetworkResponseParser : public QObject {
    Q_OBJECT
public:
    NetworkResponseParser(QObject *parent, std::shared_ptr<NetworkContext> networkContext,
                          int minSecondsBetweenSleeps = MIN_SECONDS_BETWEEN_SLEEPS,
                          int staleRequestThreshold = STALE_REQUEST_TIME,
                          int errorCoolOffTime = ERROR_COOL_OFF_TIME,
                          int hardRateLimitCoolOffTime = HARD_RATE_LIMIT_COOL_OFF_TIME);
    void setNetworkRequester(NetworkRequester* networkRequester);

private:
    std::shared_ptr<NetworkContext> networkContext;
    NetworkRequester* networkRequester;
    int minSecondsBetweenSleeps;
    int staleRequestThreshold;
    int errorCoolOffTime;
    int hardRateLimitCoolOffTime;

public slots:
    void parseResponseSlot();

public:
    void parseResponse(QNetworkReply* reply);
    void parseNetworkError(QNetworkReply* reply);
    void retryAfter(QNetworkReply* reply, int seconds, bool forceSleep=false);

private:
    void parseRateLimitError(QNetworkReply* reply);

signals:
    void fireError(std::string error, int requestId);
    void rateLimited(int seconds);
    void handleResponse(const ResponseHandlerFunc& handler, QNetworkReply* reply, const QJsonDocument& parsedResponse);
};


#endif //GITSTAT_NETWORKRESPONSEPARSER_H
