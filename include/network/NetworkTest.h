//
// Created by luka on 4.12.21..
//

#ifndef GITSTAT_NETWORKTEST_H
#define GITSTAT_NETWORKTEST_H


#include <QObject>
#include <include/network/GitLabNetworking.h>

namespace Network {
    class NetworkTest;
}

class Network::NetworkTest : public QObject {
    Q_OBJECT

public:
    NetworkTest();
    void getGroup(int id = 14033104);
    void getProject(int id = 31055519);
    void getCommits(int id = 31055519);
    void auth(const std::string& token = "aaaa");

private:
    Network::GitLabNetworking* network = GitLabNetworking::getInstance();
};


#endif //GITSTAT_NETWORKTEST_H
