//
// Created by luka on 27.12.21..
//

// TESTS:
// 1 test: handleGroupResponse - ensure doGetRequest is called
// ~3 tests: handleGroupProjectsResponse - with / without link header, groupData
// 1 test: handleProjectResponse - ensure downloadedProjectInfo is emitted
// 3 tests: handleCommitsListResponse - with / without context, with / without link header
// 1 test: handleCommitResponse - check commits are inserted, doGetRequest is invoked
// ~3 tests: handleCommitDiffResponse - with / without context, check if it's finalized properly
// =====================
// TOTAL: 12

#ifndef GITSTAT_GITLABRESPONSEHANDLER_H
#define GITSTAT_GITLABRESPONSEHANDLER_H

#include <QObject>
#include <QNetworkReply>
#include <QJsonDocument>
#include "GroupContext.h"
#include "ProjectContext.h"
#include "NetworkContext.h"
#include "NetworkRequester.h"

namespace Network {
    class GitLabResponseHandler;
}

class Network::GitLabResponseHandler : public QObject {
    Q_OBJECT

private:
    std::shared_ptr<GroupContext> groupContext;
    std::shared_ptr<ProjectContext> projectContext;
    std::shared_ptr<NetworkContext> networkContext;
    NetworkRequester* networkRequester;

    void finalizeProjectDownload(int id);

public:
    GitLabResponseHandler(QObject *parent, const std::shared_ptr<GroupContext> &groupContext,
                          const std::shared_ptr<ProjectContext> &projectContext,
                          const std::shared_ptr<NetworkContext> &networkContext, NetworkRequester *networkRequester);

    void handleGroupResponse(QNetworkReply* reply, const QJsonDocument& parsedResponse);
    void handleGroupProjectsResponse(QNetworkReply* reply, const QJsonDocument& parsedResponse);
    void handleProjectResponse(QNetworkReply* reply, const QJsonDocument& parsedResponse);
    void handleCommitsListResponse(QNetworkReply* reply, const QJsonDocument& parsedResponse);
    void handleCommitResponse(QNetworkReply* reply, const QJsonDocument& parsedResponse);
    void handleCommitDiffResponse(QNetworkReply* reply, const QJsonDocument& parsedResponse);
    void handleUserInfoResponse(QNetworkReply* reply, const QJsonDocument& parsedResponse);
    bool followLinkNext(QNetworkReply* reply, int requestId);

public slots:
    void executeResponseHandler(const ResponseHandlerFunc& handler, QNetworkReply * reply, const QJsonDocument& parsedResponse);

signals:
    void fireError(std::string error, int requestId);
    void downloadedProjectInfo(std::shared_ptr<GitLabProjectResponse> projectResponse);
    void downloadedProjectsInGroup(std::shared_ptr<GitLabGroupResponse> groupResponse);
    void authCompleted(bool successful, std::string message);
    void downloadedCommitInfo(int id, std::shared_ptr<std::list<std::shared_ptr<GitLabCommitResponse>>> commitList);
    void allRequestsFinished();
};


#endif //GITSTAT_GITLABRESPONSEHANDLER_H
