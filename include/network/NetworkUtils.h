//
// Created by luka on 27.11.21..
//

//TESTS:
// ~5 tests: parseLinkHeader(QByteArray& header) - one and multiple params, one and multiple refs, quoted and unquoted params
//    - see: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Link
// 2-3 tests: void saveAuthToken(const std::string& token, const QString& path) / std::string loadAuthToken(const QString& path = AUTH_FILE_PATH_NAME)
//    - loading non-existing file, loading existing file (?), saving and loading same file
// ~4 tests: makeQNetworkRequest(const std::string& urlString, const std::string& authToken) - URL with/without '?' part, empty and non-empty authToken
// =====================
// TOTAL: ~11

#ifndef GITSTAT_NETWORKUTILS_H
#define GITSTAT_NETWORKUTILS_H

#include <string>
#include <unordered_map>
#include <QString>
#include <QDir>
#include <QFile>
#include <QNetworkReply>

namespace Network {
    class LinkHeader;
    class GitLabResponseHandler;

    inline const char* PROP_REQUEST_ID = "request_id";
    inline const char* PROP_COMMIT_ID = "commit_id";
    inline const char* PROP_AUTH_TOKEN = "auth_token";
    inline const char* PROP_HANDLER = "handler";
    inline const char* PROP_TIME_CREATED = "time";

    inline const int ERROR_COOL_OFF_TIME = 5;
    inline const int MIN_SECONDS_BETWEEN_SLEEPS = 3;
    inline const int STALE_REQUEST_TIME = 5;
    inline const int HARD_RATE_LIMIT_COOL_OFF_TIME = 60;

    inline const std::string GET_PROJECTS_URL = "https://gitlab.com/api/v4/projects/"; // NOLINT(cert-err58-cpp)
    inline const std::string GET_GROUP_URL = "https://gitlab.com/api/v4/groups/"; // NOLINT(cert-err58-cpp)
    inline const std::string GET_COMMITS_URL_SUFFIX = "/repository/commits/"; // NOLINT(cert-err58-cpp)
    inline const std::string GET_USER_URL = "https://gitlab.com/api/v4/user"; // NOLINT(cert-err58-cpp)

    inline const QString AUTH_FILE_PATH_NAME = QDir::homePath().append("/").append("Git-stat/").append("token");
    inline const std::string TOKEN_HEADER_NAME = "PRIVATE-TOKEN"; // NOLINT(cert-err58-cpp)

    std::vector<std::shared_ptr<Network::LinkHeader>> parseLinkHeader(QByteArray& header);
    void saveAuthToken(const std::string& token, const QString& path = AUTH_FILE_PATH_NAME);
    std::string loadAuthToken(const QString& path = AUTH_FILE_PATH_NAME);
    std::string mapErrorMessage(const QNetworkReply* reply);
    bool isStatusRateLimited(const QVariant& statusCode);
    std::chrono::duration<long>::rep getUnixTimestamp();
    std::shared_ptr<QNetworkRequest> makeQNetworkRequest(const std::string& urlString, const std::string& authToken="");
    std::string makeUniqueId(QNetworkReply* reply);
    bool shouldErrorBeRetried(QNetworkReply* reply);

    using ResponseHandlerFunc = void (Network::GitLabResponseHandler::*)(QNetworkReply* reply, const QJsonDocument&);
}

Q_DECLARE_METATYPE(Network::ResponseHandlerFunc)

//as per https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Link
class Network::LinkHeader {
public:
    QString uriReference;
    std::unordered_map<QString, QString> params;
};

#endif //GITSTAT_NETWORKUTILS_H
