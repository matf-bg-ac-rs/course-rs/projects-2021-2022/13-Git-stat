//
// Created by caca on 25.12.21..
//

#ifndef GITSTAT_PROCESSINGCOMMIT_H
#define GITSTAT_PROCESSINGCOMMIT_H

#include "Commit.h"
#include "include/network/GitLabCommitResponse.h"
#include "include/data/ProcessingComments.h"

class ProcessingCommit: public QObject{
Q_OBJECT

private:
    static ProcessingCommit* instance;
    explicit ProcessingCommit(QObject *parent = nullptr);

public:
    static ProcessingCommit* getInstance();

    static QSharedPointer<Commit> commitFromQJsonToClass(const QJsonObject& obj);
    static QJsonObject commitFromClassToQJson(const QSharedPointer<Commit>& commits);

    static QSharedPointer<Commit> parseToCommitCommitNetworkResponse(int projectId, const std::shared_ptr<Network::GitLabCommitResponse>& commitInfo);
    static QJsonArray createQJsonFromCommitMap(const QSharedPointer<QMap<Author, QVector<QSharedPointer<Commit>>>>& commitMap);
    static QSharedPointer<QMap<Author, QVector<QSharedPointer<Commit>>>> createCommitMapFromQJson(const QJsonArray& commitMap, int repoId) ;

private:
};


#endif //GITSTAT_PROCESSINGCOMMIT_H