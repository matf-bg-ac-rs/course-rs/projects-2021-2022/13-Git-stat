//
// Created by caca on 25.12.21..
//

#ifndef GITSTAT_PROCESSINGREPO_H
#define GITSTAT_PROCESSINGREPO_H

#include "include/data/ProcessingComments.h"
#include "include/data/ProcessingCommit.h"
#include "include/network/GitLabNetworking.h"
#include <QJsonValue>
#include <QJsonArray>
#include <vector>
#include "Repo.h"
#include <filesystem>
#include <QDir>
#include <QSharedPointer>


class ProcessingRepo: public QObject{
Q_OBJECT

private:
    static ProcessingRepo* instance;
    explicit ProcessingRepo(QObject *parent = nullptr);

public:
    static ProcessingRepo* getInstance();

    static const inline QString DIR_PATH = QDir::homePath().append("/").append("Git-stat/").append("dataInfo");
    static const inline QString FILE_NAME = DIR_PATH + "/repos.json";


    QSharedPointer <Repo> parseRepoFromQJsonToClass(QJsonObject obj) const;
    static QJsonObject parseRepoFromClassToQJson(const QSharedPointer <Repo> &r) ;

    QSharedPointer<QVector<QSharedPointer<Repo>>> readAllReposFromDisk(const QString& path = FILE_NAME) const;
    void writeAllReposToDisk(const QSharedPointer<QVector<QSharedPointer<Repo>>>& repos,const QString& path = FILE_NAME) ;

    static QSharedPointer<Repo> parseToRepoProjectNetworkResponse(const std::shared_ptr<Network::GitLabProjectResponse>& projectInfo);

    static void setCommitStatsForRepo(QSharedPointer<Repo> repo, const QSharedPointer<QMap<Author, QVector<QSharedPointer<Commit>>>>& commitsMap);

    ProcessingComments *commentProcessor;
    ProcessingCommit *commitProcessor;
};

#endif //GITSTAT_PROCESSINGREPO_H
