#ifndef COMMENT_H
#define COMMENT_H

#include <iostream>
#include <QString>
#include <QDate>

enum class TypeOfComm{
    author,commit,repo
};

class Comment{

    public:
        Comment( const TypeOfComm &type, QString parentId, QDateTime createdAt, QString  message, int repoId);

        TypeOfComm getTypeOfComm() const;
        static TypeOfComm getTypeOfComm(const QString& typeCom);
        QString getParentId() const;
        QDateTime getCreatedAt() const;
        QString getMessage() const;
        QString typeToString() const;
        int getRepoId() const;


    bool operator<(const Comment &comm) const;
    bool operator==(const Comment &comm) const;

    private:
        TypeOfComm typeComment;
        QString parentId; // author-email/repoId/groupId
        QDateTime createdAt;
        QString message;
        int repoId;
};

#endif // COMMENT_H
