#ifndef AUTHOR_H
#define AUTHOR_H

#include <vector>
#include<iostream>
#include <QString>
#include <QVector>
#include "Comment.h"

class Author{

    public:
        Author(QString name,  QString email, QVector<QSharedPointer<Comment>> com={});
        Author();

        QString getName() const;
        QString getEmail() const;
        QVector<QSharedPointer<Comment>> getComments() const;
        void addComment(QSharedPointer<Comment>& comm);
        void setComments( QVector<QSharedPointer<Comment>> comm);

    private:
        QString name;
        QString email;
        QVector<QSharedPointer<Comment>> comments;
};


inline bool operator<(const Author &a, const Author &b)
{
    if (a.getEmail() != b.getEmail())
        return a.getEmail() < b.getEmail();
    return false;
}

inline bool operator==(const Author &a, const Author &b)
{
    return a.getEmail() == b.getEmail();
}

#endif // AUTHOR_H
