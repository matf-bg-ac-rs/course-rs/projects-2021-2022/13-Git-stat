#ifndef COMMIT_H
#define COMMIT_H

#include <iostream>
#include <vector>
#include <string>
#include "Author.h"
#include "include/network/GitLabCommitResponse.h"

class Commit{

    public:
        struct CommitStats{
            bool operator==(const CommitStats &rhs) const;

            bool operator!=(const CommitStats &rhs) const;

            int additions;
            int deletions;
            int total;
        };
        struct FileChange {
            QString oldFilename;
            QString newFilename;
            bool isFileCreated;
            bool isFileRenamed;
            bool isFileDeleted;

            explicit FileChange(Network::GitLabCommitResponse::FileChange &fc);
            FileChange(QString old, QString newf, bool creat, bool rename, bool deleted);

            bool operator==(const FileChange &rhs) const;

            bool operator!=(const FileChange &rhs) const;
        };

        Commit(QString id,QDateTime createdAt,
            QString title,
            QString message, Author author,
            QString webUrl, const CommitStats &stats,
            QVector<FileChange> changes, QVector<QSharedPointer<Comment>> comm={});

        ~Commit();
        Commit& operator=(const Commit& commit);
        Commit(Commit& commit);

        const QString &getId() const;
        const QDateTime &getCreatedAt() const;
        const QString &getTitle() const;
        const QString &getMessage() const;
        const Author &getAuthor() const;
        const  QString &getWebUrl() const;
        const CommitStats &getStats() const;
        const QVector<FileChange> &getFileChanges() const;
        QVector<QSharedPointer<Comment>> getComments() const;
        void addComment(const QSharedPointer<Comment>& comm);
        void setComment(QVector<QSharedPointer<Comment>> comm);

    bool operator==(const Commit &rhs) const;

    bool operator!=(const Commit &rhs) const;

    private:
        QString id;
        Author author;  //name,email
        QDateTime createdAt;
        QString title;
        QString message;
        QString webUrl;
        CommitStats stats;
        QVector<FileChange> changes;
        QVector<QSharedPointer<Comment>> comments;
};

inline bool operator<(const Commit &a, const Commit &b)
{
    return a.getCreatedAt() < b.getCreatedAt();
}

#endif // COMMIT_H
