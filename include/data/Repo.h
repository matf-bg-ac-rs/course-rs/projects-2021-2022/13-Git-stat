#ifndef REPO_H
#define REPO_H

#include <iostream>
#include <vector>
#include <map>
#include <memory>
#include "Author.h"
#include "Commit.h"

class Commit;

class Repo
{
    public:
        Repo(int _id, QString _name, QDateTime _createdAt,
             QString _webUrl, QString _description, QDateTime _lastActivity,
             int _numOfCommits=0, int _numOfAuthors=0,
             QSharedPointer<QMap<Author,QVector<QSharedPointer<Commit>>>> _commitsPerAuthor=
                QSharedPointer<QMap<Author,QVector<QSharedPointer<Commit>>>>::create() ,
             int _groupId=-1,QString _groupName="", QVector<QSharedPointer<Comment>> comm={});

        int getId() const;
        QString getName()  const;
        QDateTime getCreatedAt() const;
        QSharedPointer<QMap<Author, QVector<QSharedPointer<Commit>>>> getCommitsPerAuthor() const;
        int getNumOfAuthors() const;
        int getNumOfCommits() const;
        int getGroupId() const;
        QString getGroupName() const;
        QString getDescription() const;
        QString getWebUrl() const;
        QDateTime getLastActivity() const;
        void setNumOfCommits(int num);
        void setNumOfAuthors(int num);
        void setCommits(QSharedPointer<QMap<Author, QVector<QSharedPointer<Commit>>>> comm);
        QVector<QSharedPointer<Comment>> getComments() const;
        void addComment(const QSharedPointer<Comment>& comm);
        void setComments(QVector<QSharedPointer<Comment>> comm);

    bool operator==(const Repo &rhs) const;
    bool operator!=(const Repo &rhs) const;

    private:
        int id;
        QString webUrl;
        QString description;
        QDateTime lastActivity;
        QDateTime createdAt;
        QString name;
        int numOfCommits;
        int numOfAuthors;
        QSharedPointer<QMap<Author, QVector<QSharedPointer<Commit>>>> commitsPerAuthor;
        int groupId;
        QString groupName;
        QVector<QSharedPointer<Comment>> comments;
};

#endif // REPO_H


