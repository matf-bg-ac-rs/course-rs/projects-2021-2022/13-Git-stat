//
// Created by caca on 25.12.21..
//

#ifndef GITSTAT_PROCESSINGCOMMENTS_H
#define GITSTAT_PROCESSINGCOMMENTS_H

#include <QJsonValue>
#include <QJsonArray>
#include <vector>
#include "Comment.h"
#include <filesystem>
#include <QDir>
#include <QSharedPointer>
#include "Repo.h"


class ProcessingComments: public QObject{
Q_OBJECT

private:
    static ProcessingComments* instance;
    explicit ProcessingComments(QObject *parent = nullptr);

    static const inline QString DIR_PATH = QDir::homePath().append("/").append("Git-stat/").append("dataInfo");
    static const inline QString FILE_NAME = DIR_PATH + "/comments.json";

public:
    static ProcessingComments* getInstance();

    static void removeCommentsForRepoId(int id, const QString& fileName = FILE_NAME);
    static QVector<QSharedPointer<Comment>> getCommentsForObjectId(const QString& id, TypeOfComm tip, int repoId, const QString& fileName = FILE_NAME);
    void writeCommentToDisk(const QSharedPointer<Comment>& comment, const QString& fileName = FILE_NAME);

    static QSharedPointer<Comment> parseCommentQJsonToClass(QJsonObject comment);
    static QJsonObject parseCommentClassToQJson(const QSharedPointer<Comment>& comment);

    void addCommentToRepo(QSharedPointer<Repo> repo, QSharedPointer<Comment> comment, QString& email);

private:
    QDir dataDir;

};
#endif //GITSTAT_PROCESSINGCOMMENTS_H
