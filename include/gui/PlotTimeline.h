#ifndef PLOT_TIMELINE_H
#define PLOT_TIMELINE_H

#define X_START 300
#define X_END 1300
#define Y_START 50

#include "include/gui/MonthNode.h"
#include "include/data/Commit.h"
#include "include/gui/AuthorNode.h"
#include "include/gui/CommitNode.h"
#include "include/gui/SingleRepoData.h"
#include "include/gui/CommitNode.h"
#include "include/gui/CheckBoxList.h"
#include <QDateTime>
#include <QtAlgorithms>
#include <QCalendar>
#include <QLocale>
#include <QObject>

namespace Ui {class MainWindow;}

class PlotTimeline : public QObject
{
    Q_OBJECT
public:
    PlotTimeline(Ui::MainWindow* ui, QGraphicsScene* scene, QSharedPointer<SingleRepoData> data, CheckBoxList* cb);
    ~PlotTimeline();

    void create_authors_nodes_and_timelines(int i, int counter);
    void create_commits_on_timeline(QVector<QSharedPointer<Commit>> commits, int months, float y_coord);
    std::vector<CommitNode> calculate_position_of_commits_on_timeline();
    void plot_vertical_lines(int index, int counter, int numOfAuthors);
    QColor calculate_color(int n);

    void delete_vertical_lines();
    void delete_horizontal_lines_and_authors();
    void delete_commits();
    void delete_months();

    void calculate_vector_of_months();

    Ui::MainWindow* getUi();


public slots:
    void selected_authors(QVector<bool> checked_fields);
    void chosen_timeline_for_plot(int index);
    void plot_months_name();

private:
    Ui::MainWindow* ui;
    QGraphicsScene* scene;
    QSharedPointer<SingleRepoData> data;

    CheckBoxList* cb;
    QVector<AuthorNode *> _authors;
    QVector<QGraphicsLineItem*> horizontal_lines;
    QVector<QGraphicsLineItem*> vertical_lines;
    QVector<QVector<CommitNode*>> _commits;
    QVector<int> chosen_authors;
    int numOfMonths;
    int currentMonth;

    QLocale local;  //format za datum -> americki c()
    QVector<MonthNode* > _months;

};

#endif // PLOT_TIMELINE_H
