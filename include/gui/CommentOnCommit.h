#ifndef COMMENT_ON_COMMIT_H
#define COMMENT_ON_COMMIT_H

#include <QDialog>

namespace Ui {
class CommentOnCommit;
}

class CommentOnCommit : public QDialog
{
    Q_OBJECT

public:
    explicit CommentOnCommit(QWidget *parent = nullptr);
    ~CommentOnCommit();

    Ui::CommentOnCommit* getUi();

private:
    Ui::CommentOnCommit *ui;
};

#endif // COMMENT_ON_COMMIT_H
