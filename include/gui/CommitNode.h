#ifndef COMMITNODE_H
#define COMMITNODE_H

#define NODE_DIAMETER 13

#include <QGraphicsItem>
#include <QPainter>
#include <QBrush>
#include <QSharedPointer>
#include <QGraphicsView>
#include <QGraphicsScene>
#include "include/data/Commit.h"

class CommitNode : public QGraphicsItem
{
public:
    CommitNode(QVector<QSharedPointer<Commit>> *commits);

    QRectF boundingRect() const override;
    void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget = nullptr) override;

    void setBrush(QColor color);
    QBrush getBrush();
    QVector<QSharedPointer<Commit>>* getCommits();
    qint32 Diameter() const;

private:
    QVector<QSharedPointer<Commit>> *commits;
    QBrush brush;
};

#endif // COMMITNODE_H
