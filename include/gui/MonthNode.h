#ifndef MONTHNODE_H
#define MONTHNODE_H

#include <QString>
#include <QGraphicsItem>


class MonthNode : public QGraphicsItem{

public:
    MonthNode();

    MonthNode(QString name);

    QRectF boundingRect() const override;
    void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget = nullptr) override;

    QString getName();
    qint32 Height() const;
    qint32 Width() const;
private:
    QString name;
};

#endif // MONTHNODE_H
