#ifndef CHECK_BOX_LIST_H
#define CHECK_BOX_LIST_H

//#include "include/gui/plot_timeline.h"
#include "include/gui/SingleRepoData.h"
#include <QCheckBox>
#include <QGraphicsScene>


namespace Ui {class MainWindow;}

class CheckBoxList: public QObject
{
    Q_OBJECT
public:
    CheckBoxList(Ui::MainWindow* ui, QGraphicsScene* scene, QSharedPointer<SingleRepoData> data);
    ~CheckBoxList();

    void create_check_boxes();
    QVector<QCheckBox*> getCheckboxes();

signals:
    void authors_are_selected(QVector<bool> checked_fields);

public slots:
    void pb_choose_clicked();

private:
    Ui::MainWindow* ui;
    QGraphicsScene* scene;
    QSharedPointer<SingleRepoData> data;
    QVector<QCheckBox*> checkboxes;

};

#endif // CHECK_BOX_LIST_H
