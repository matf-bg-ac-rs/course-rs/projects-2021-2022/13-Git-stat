#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSharedPointer>
#include <QWidget>
#include "Scene.h"
#include "include/gui/SingleRepoPage.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow() override;

    Ui::MainWindow* getUi();
    Scene* getScene();
    QSharedPointer<SingleRepoData> getData();

    void show_content_of_the_second_page(PlotTimeline* plot_timeline, CheckBoxList* check_box, SingleRepoPage* page_two);

public slots:
    void on_pbToken_clicked();
    void on_pbAdd_clicked();
    void seeMoreDetails(QSharedPointer<Repo> repo);

private:
    Ui::MainWindow* ui;
    Scene* scene;
};
#endif // MAINWINDOW_H
