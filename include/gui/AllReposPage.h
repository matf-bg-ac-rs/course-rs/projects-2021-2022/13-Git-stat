#ifndef PAGE_ONE_H
#define PAGE_ONE_H

#include "include/gui/MainWindow.h"
#include "include/gui/RepoNode.h"
#include "include/gui/NetworkResponseHandler.h"
#include "include/data/Repo.h"
#include "include/data/DataProcessing.h"

#include <iostream>
#include <memory>
#include <string>
#include <QVector>
#include <QObject>

class NetworkResponseHandler;

namespace Ui {class MainWindow;}

class AllReposPage : public QObject
{

    Q_OBJECT

public:
    AllReposPage(Ui::MainWindow *ui, MainWindow* w);

    void showRepos();

    void setRepoi(const QVector<RepoNode *> &newRepoi);
    const QVector<RepoNode *> &getRepoi() const;
    void setMessage(QString message);
    QVector<int> calculateRowAndColumn();

public slots:
    void addNewRepository(QSharedPointer<Repo> repo);
    void refreshAllRepositories();
    void allRepoInfoReady(QSharedPointer<Repo> repo);
    void removeRepository(RepoNode *r);
    void removeAllRepositories();

private:

    Ui::MainWindow *ui;
    MainWindow* w;
    QVector<RepoNode*>repoi;
    QSharedPointer<QVector<QSharedPointer<Repo>>> rs;
    DataProcessing *dp;
    NetworkResponseHandler *nh;
    int row = 0;

    void removeAllRepositoriesFromGrid();
    void addRepoToGrid(RepoNode *r);

};

#endif // PAGE_ONE_H

