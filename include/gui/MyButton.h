#ifndef MYBUTTON_H
#define MYBUTTON_H

#include <QPushButton>
#include <QDebug>

class MyButton : public QPushButton
{
    Q_OBJECT
public:
    MyButton(QString text);

signals:
    void my_button_clicked(int i);

private slots:
    void reemitClicked();
};

#endif // MYBUTTON_H
