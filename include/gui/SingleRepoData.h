#ifndef SINGLE_REPO_DATA_H
#define SINGLE_REPO_DATA_H

#include "../data/Author.h"
#include "../data/Repo.h"
#include <QVector>


class SingleRepoData{

public:
    SingleRepoData(QSharedPointer<Repo> repo);

    QVector<Author> getAuthors();

    QSharedPointer<Repo> getRepo();
    QVector<QSharedPointer<Commit>> findCommits(Author author);

private:
    QSharedPointer<Repo> repo;
};

#endif // SINGLE_REPO_DATA_H
