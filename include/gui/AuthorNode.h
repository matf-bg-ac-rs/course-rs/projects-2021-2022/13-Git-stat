#ifndef AUTHORNODE_H
#define AUTHORNODE_H

#define NODE_HEIGHT 100
#define NODE_WIDTH 300

#include <QGraphicsItem>
#include <QString>
#include <iostream>
#include "include/data/Author.h"
#include "include/data/Commit.h"

class AuthorNode : public QGraphicsItem{

public:
    AuthorNode();

    AuthorNode(Author a);

    QRectF boundingRect() const override;
    void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget = nullptr) override;

    qint32 Height() const;
    qint32 Width() const;

    Author getAuthor();

private:
    Author author;
};

#endif // AUTHORNODE_H
