#ifndef POPUPADDDIALOG_H
#define POPUPADDDIALOG_H

#include <QDialog>
#include "include/data/DataProcessing.h"

namespace Ui { class PopupAddDialog;}

class PopupAddDialog : public QDialog
{
    Q_OBJECT

public:
    explicit PopupAddDialog(QWidget *parent = nullptr);
    ~PopupAddDialog();

public slots:
    void AddNewRepositoryOrGroup();
    void showPopupTokenDialog();

private:
    Ui::PopupAddDialog *ui;
    DataProcessing *dp;
};

#endif // POPUPADDDIALOG_H
