#ifndef REPONODE_H
#define REPONODE_H

#include "include/gui/MainWindow.h"
#include "include/data/DataProcessing.h"
#include "include/data/Repo.h"

#include <string>
#include <memory>
#include <QWidget>
#include <QSharedPointer>

namespace Ui {class RepoNode;}

class RepoNode : public QWidget
{
    Q_OBJECT

public:
    explicit RepoNode(QWidget *parent = nullptr);
    ~RepoNode();

    Ui::RepoNode *getUi() const;
    int getRow() const;
    int getColumn() const;
    QSharedPointer<Repo> getRepo() const;

    void setUrlAsLink(QString url);
    void setRow(int newRow);
    void setColumn(int newColumn);
    void setRepo(QSharedPointer<Repo> newRepo);
    void setWindow(MainWindow* w);
    void setNode();

public slots:
    void pbRefreshRepositoryClicked();
    void pbSeeMoreDetailsClicked();
    void pbRemoveClicked();

signals:
    void seeMoreDetails(QSharedPointer<Repo> repo);
    void removeRepository(RepoNode* r);

private:
    Ui::RepoNode *ui;
    DataProcessing *dp;
    QSharedPointer<Repo> repo;
    MainWindow* w;

    int row;
    int column;
};

#endif // REPONODE_H
