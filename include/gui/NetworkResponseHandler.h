#ifndef NETWORKRESPONSEHANDLER_H
#define NETWORKRESPONSEHANDLER_H

#include "include/gui/MainWindow.h"
#include "include/network/GitLabNetworking.h"

namespace Ui {class MainWindow;}
namespace Network {class GitLabNetworking;}

class NetworkResponseHandler : public QObject
{
    Q_OBJECT

public:
    NetworkResponseHandler(Ui::MainWindow *ui);
    void setMessage(QString message);

public slots:
    void authorization(bool valid, std::string text);
    void downloadError(std::string error);
    void allRequestsFinished();
    void rateLimited(int timeoutSeconds);
    void clearMessage();
    void countDown();

private:
    Ui::MainWindow *ui;
    QSharedPointer <QTimer> qt;
    int tmt = 0;
};

#endif // NETWORKRESPONSEHANDLER_H
