#include "include/gui/PopupAddDialog.h"
#include "include/gui/PopupTokenDialog.h"
#include "ui_PopupAddDialog.h"

PopupAddDialog::PopupAddDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PopupAddDialog),
    dp(DataProcessing::getInstance())
{
    ui->setupUi(this);
    ui->rbRepo->setChecked(true);

    connect(ui->bbRepoGroup,&QDialogButtonBox::accepted,this,&PopupAddDialog::AddNewRepositoryOrGroup);
    connect(ui->pbToken,&QPushButton::clicked,this,&PopupAddDialog::showPopupTokenDialog);

}

PopupAddDialog::~PopupAddDialog()
{
    delete ui;
}

void PopupAddDialog::AddNewRepositoryOrGroup()
{
    int checked = 0;
    int id = ui->leId->text().toInt();

    if(ui->rbRepo->isChecked()){
       checked = 0;
    } else {
       checked = 1;
    }

    if (checked==1){
        dp->getGroupInfo(id);
    } else {
        dp->getProjectInfo(id,false);
    }
}

void PopupAddDialog::showPopupTokenDialog()
{
    PopupTokenDialog t;
    t.exec();
}
