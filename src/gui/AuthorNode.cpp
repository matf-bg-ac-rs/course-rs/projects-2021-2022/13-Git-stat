#include "include/gui/AuthorNode.h"
#include <QPainter>

AuthorNode::AuthorNode()
    :QGraphicsItem()
{

}

AuthorNode::AuthorNode(Author a)
    : QGraphicsItem()
    ,  author(a)
{
    setFlag(ItemIsSelectable);
}

QRectF AuthorNode::boundingRect() const
{
    return QRectF(0, 0, Width(), Height());
}

void AuthorNode::paint(QPainter* painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    painter->fillRect(boundingRect(), Qt::white);
    painter->setPen(Qt::black);

    auto tekst = author.getName() + "\n" + author.getEmail();
    painter->drawText(boundingRect(), Qt::AlignHCenter | Qt::AlignVCenter, tekst);

}

qint32 AuthorNode::Height() const
{
    return NODE_HEIGHT;
}

qint32 AuthorNode::Width() const
{
    return NODE_WIDTH;
}

Author AuthorNode::getAuthor()
{
    return author;
}
