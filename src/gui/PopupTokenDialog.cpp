#include "include/gui/PopupTokenDialog.h"
#include "ui_PopupTokenDialog.h"

PopupTokenDialog::PopupTokenDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PopupTokenDialog)
{
    ui->setupUi(this);

    connect(ui->bbToken,&QDialogButtonBox::accepted,this,&PopupTokenDialog::AddToken);

}

PopupTokenDialog::~PopupTokenDialog()
{
    delete ui;
}

void PopupTokenDialog::AddToken()
{
    QString token = ui->leToken->text();
    Network::GitLabNetworking *gl = Network::GitLabNetworking::getInstance();
    gl->setAuthToken(token.toStdString());
}
