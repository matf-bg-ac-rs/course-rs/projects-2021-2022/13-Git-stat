#include "include/gui/CommentOnCommit.h"
#include "ui_CommentOnCommit.h"

CommentOnCommit::CommentOnCommit(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CommentOnCommit)
{
    ui->setupUi(this);
}

CommentOnCommit::~CommentOnCommit()
{
    delete ui;
}

Ui::CommentOnCommit *CommentOnCommit::getUi()
{
    return ui;
}
