#include "include/gui/SingleRepoPage.h"
#include "ui_MainWindow.h"
#include "ui_CommentOnRepo.h"
#include "ui_CommentOnAuthor.h"
#include "ui_CommentOnCommit.h"

#include <QDesktopServices>
#include <QUrl>
#include <QLineEdit>
#include <QPlainTextEdit>

SingleRepoPage* SingleRepoPage::instance{};

SingleRepoPage *SingleRepoPage::getInstance()
{
    if(SingleRepoPage::instance == nullptr){
        SingleRepoPage::instance = new SingleRepoPage();
    }

    return SingleRepoPage::instance;
}

SingleRepoPage::SingleRepoPage()
{
    qDebug() << "page_two je kreran";
}

void SingleRepoPage::connections()
{
    connect(ui->pb_choose, &QPushButton::clicked, check_box, &CheckBoxList::pb_choose_clicked);
    connect(check_box, &CheckBoxList::authors_are_selected, plot_timeline, &PlotTimeline::selected_authors);
    connect(this, &SingleRepoPage::chosen_timeline, plot_timeline, &PlotTimeline::chosen_timeline_for_plot);
}

void SingleRepoPage::connect_once()
{
    connect(ui->pb_back_to_projects, &QPushButton::clicked, this, &SingleRepoPage::pb_back_to_projects_clicked);
    connect(ui->cb_chose_timeline, &QComboBox::activated, this, &SingleRepoPage::cb_chose_timeline_activated);
    connect(ui->pb_send_an_email, &QPushButton::clicked, this, &SingleRepoPage::send_an_email);
    connect(ui->pb_add_comment_for_repo, &QPushButton::clicked, this, &SingleRepoPage::pb_add_comment_for_repo_clicked);
    connect(scene, &Scene::author_node_clicked, this, &SingleRepoPage::author_node_clicked);
    connect(scene, &Scene::commit_node_clicked, this, &SingleRepoPage::commit_node_clicked);
    connect(ui->pb_report, &QPushButton::clicked, this, &SingleRepoPage::create_report_clicked);
}


void SingleRepoPage::setUi(Ui::MainWindow *ui)
{
    this->ui = ui;
}

void SingleRepoPage::setScene(Scene *scene)
{
    this->scene = scene;
}

void SingleRepoPage::setData(QSharedPointer<SingleRepoData> data)
{
    this->data = data;
}

void SingleRepoPage::setCheckBox(CheckBoxList *cb_l)
{
    this->check_box = cb_l;
}

void SingleRepoPage::setPlotTimeline(PlotTimeline *pt)
{
    this->plot_timeline = pt;
}

void SingleRepoPage::draw_on_a_graphics_view()
{
    ui->gv_1->setScene(scene);
    ui->gv_1->setAlignment(Qt::AlignTop | Qt::AlignLeft);
}


void SingleRepoPage::commit_node_clicked(QVector<QSharedPointer<Commit>> *commits)
{
    this->commits = commits;
    ui->scrollArea_commitInfo->setVisible(true);

    auto layout = qobject_cast<QFormLayout*>(ui->commit_info_layout->layout());
    int rows_commit = layout->rowCount();

    for(int i=0; i<rows_commit; i++){
        layout->removeRow(0);
    }

    QLabel* lbl_number = new QLabel("Number of commits: " + QString::number(commits->size()));
    lbl_number->setFrameStyle(QFrame::Panel | QFrame::Raised);
    layout->addRow(lbl_number);

    for(int i=0; i<commits->size(); i++){
        QString createdAt = commits->at(i)->getCreatedAt().toString();
        QString message = commits->at(i)->getMessage();

        QLabel* lbl_created_at = new QLabel("Created at: " + createdAt);

        QLabel* lbl_message = new QLabel("Message: ");
        QLabel* le_message = new QLabel(message.left(message.size()-1));

        int additions = commits->at(i)->getStats().additions;
        int deletions = commits->at(i)->getStats().deletions;
        int total = commits->at(i)->getStats().total;

        QLabel* lbl_add = new QLabel("Additions: ");
        QLabel* le_add = new QLabel(QString::number(additions));
        QLabel* lbl_del = new QLabel("Deletions: ");
        QLabel* le_del = new QLabel(QString::number(deletions));
        QLabel* lbl_tot = new QLabel("Total: ");
        QLabel* le_tot = new QLabel(QString::number(total));

        layout->addRow(lbl_created_at);
        layout->addRow(lbl_message, le_message);
        layout->addRow(lbl_add, le_add);
        layout->addRow(lbl_del, le_del);
        layout->addRow(lbl_tot, le_tot);

        if(commits->at(i)->getFileChanges().size() > 0){
            QLabel* lbl_changes = new QLabel("Changes: ");
            layout->addRow(lbl_changes);
        }

        for(int j=0; j<commits->at(i)->getFileChanges().size(); j++){
            Commit::FileChange file_change = commits->at(i)->getFileChanges().at(j);

            QLabel* lbl_file_name = new QLabel();
            if(file_change.isFileRenamed){
                lbl_file_name->setText("  • " + file_change.oldFilename + " ➡ " + file_change.newFilename);
            }
            else if(file_change.isFileCreated){
                lbl_file_name->setText("  • " + file_change.oldFilename + " (created)");
            }
            else{
                lbl_file_name->setText("  • " + file_change.oldFilename + " (deleted)");
            }
            layout->addRow(lbl_file_name);
        }

        if(commits->at(i)->getComments().size() > 0){
            QLabel* lbl_comments = new QLabel("Comments: ");
            layout->addRow(lbl_comments);

            for(auto &el : commits->at(i)->getComments()){
                qDebug() << "uslo" << el->getMessage();
                QLabel* lbl_tmp_comment = new QLabel("  • " + el->getMessage());
                layout->addRow(lbl_tmp_comment);
            }
        }

        MyButton* button = new MyButton("Add a comment for commit " + QString::number(i+1));
        button->setMaximumWidth(405);
        layout->addRow(button);

        connect(button, &MyButton::my_button_clicked, this, &SingleRepoPage::pb_add_comment_for_commit_clicked);

        QLabel* lbl_vline_1 = new QLabel("");
        lbl_vline_1->setFrameStyle(QFrame::HLine | QFrame::Plain);
        layout->addRow(lbl_vline_1);
    }
}


void SingleRepoPage::create_report_clicked()
{
    emit create_report(this->data->getRepo()->getId());
    QMessageBox message;
    message.setText("You have successfully created a report. It is available in ~/Git-stat");
    message.setObjectName("message");
    message.setStyleSheet("#message {background-color: rgb(203, 228, 235)}");
    message.exec();
}


void SingleRepoPage::create_repo_info_box()
{
    QSharedPointer<Repo> repo = data->getRepo();


    auto layout = qobject_cast<QFormLayout*>(ui->repo_info_layout->layout());


    QLineEdit* le_id = new QLineEdit(QString::number(repo->getId()));
    le_id->setObjectName("id");
    le_id->setStyleSheet("#id {background-color: rgb(203, 228, 235)}");
    QLabel* lbl_id = new QLabel("Id:");
    le_id->setReadOnly(true);
    layout->addRow(lbl_id, le_id);

    QTextEdit* le_name = new QTextEdit(repo->getName());
    le_name->setObjectName("name");
    le_name->setStyleSheet("#name {background-color: rgb(203, 228, 235)}");
    QLabel* lbl_name = new QLabel("Name:");
    le_name->setReadOnly(true);
    le_name->setMaximumHeight(50);
    layout->addRow(lbl_name, le_name);

    if(repo->getDescription().size() > 0){
        QTextEdit* le_desc = new QTextEdit(repo->getDescription());
        le_desc->setObjectName("desc");
        le_desc->setStyleSheet("#desc {background-color: rgb(203, 228, 235)}");
        QLabel* lbl_desc = new QLabel("Description");
        le_desc->setReadOnly(true);
        layout->addRow(lbl_desc, le_desc);
    }


    QLineEdit* le_created_at = new QLineEdit(repo->getCreatedAt().toString());
    le_created_at->setObjectName("created");
    le_created_at->setStyleSheet("#created {background-color: rgb(203, 228, 235)}");
    QLabel* lbl_created_at = new QLabel("Created at:");
    le_created_at->setReadOnly(true);
    layout->addRow(lbl_created_at, le_created_at);

    QLineEdit* le_last_activity = new QLineEdit(repo->getLastActivity().toString());
    le_last_activity->setObjectName("activity");
    le_last_activity->setStyleSheet("#activity {background-color: rgb(203, 228, 235)}");
    QLabel* lbl_last_activity = new QLabel("Last activity:");
    le_last_activity->setReadOnly(true);
    layout->addRow(lbl_last_activity, le_last_activity);

    QLineEdit* le_commits = new QLineEdit(QString::number(repo->getNumOfCommits()));
    le_commits->setObjectName("commits");
    le_commits->setStyleSheet("#commits {background-color: rgb(203, 228, 235)}");
    QLabel* lbl_commits = new QLabel("Number of commits:");
    le_commits->setReadOnly(true);
    layout->addRow(lbl_commits, le_commits);

    if(repo->getGroupId() != -1){
        QLineEdit* le_groupId = new QLineEdit(QString::number(repo->getGroupId()));
        le_groupId->setObjectName("group");
        le_groupId->setStyleSheet("#group {background-color: rgb(203, 228, 235)}");
        QLabel* lbl_groupId = new QLabel("Group id:");
        le_groupId->setReadOnly(true);
        layout->addRow(lbl_groupId, le_groupId);

    }

    if(repo->getComments().size() > 0){
        QLabel* lbl_comment = new QLabel("Comments:");
        QTextEdit* le_comment_tmp = new QTextEdit();
        le_comment_tmp->setReadOnly(true);
        le_comment_tmp->setObjectName("comment");
        le_comment_tmp->setStyleSheet("#comment {background-color: rgb(203, 228, 235)}");

        QString tmp_text = "";
        for(auto &el : repo->getComments()){
            tmp_text.append("• " + el->getMessage()).append("\n");
        }
        tmp_text = tmp_text.left(tmp_text.size()-1);
        le_comment_tmp->setText(tmp_text);
        layout->addRow(lbl_comment, le_comment_tmp);

    }
}


void SingleRepoPage::send_an_email()
{
    QString subject = "[RS_PROJEKAT-2021/2022]";
    QString body = "...";
    QString to = "";

    for(int i=0; i<data->getRepo()->getCommitsPerAuthor()->keys().size()-1; i++){
        to.append(data->getAuthors()[i].getEmail());
        to.append(",");
    }

    int last = data->getAuthors().size()-1;

    to.append(data->getAuthors()[last].getEmail());

    QDesktopServices::openUrl(QUrl("mailto:" + to + "?subject=" + subject + "&body=" + body, QUrl::TolerantMode));

}



void SingleRepoPage::select_duration_of_the_timeline()
{
    for(int i=0; i<12; i++){
        if(i == 0){
            ui->cb_chose_timeline->addItem(QString::number(i+1) + " month");
        }
        else{
            ui->cb_chose_timeline->addItem(QString::number(i+1) + " months");
        }
    }

}


void SingleRepoPage::cb_chose_timeline_activated()
{
    int index = ui->cb_chose_timeline->currentIndex();

    emit chosen_timeline(index);
}


void SingleRepoPage::pb_repo_comment_clicked()
{
    TypeOfComm type = TypeOfComm::repo;
    QString parentId = QString::number(data->getRepo()->getId());
    QDateTime createdAt = QDateTime::currentDateTime();
    QString message = comment_repo->getUi()->pte_repo_comment->toPlainText();
    int repoId = data->getRepo()->getId();

    QString str="";
    emit comment_created(QSharedPointer<Comment>::create(type, parentId, createdAt, message, repoId), str);
    comment_repo->done(0);

    auto layout_repo = qobject_cast<QFormLayout*>(ui->repo_info_layout->layout());
    delete_layout_content(layout_repo);
    create_repo_info_box();
}

void SingleRepoPage::pb_add_comment_for_repo_clicked()
{
    comment_repo = new CommentOnRepo();
    comment_repo->open();

    connect(comment_repo->getUi()->pb_repo_comment, &QPushButton::clicked, this, &SingleRepoPage::pb_repo_comment_clicked);
}


void SingleRepoPage::pb_author_comment_clicked()
{

    TypeOfComm type = TypeOfComm::author;
    QString parentId = author.getEmail();
    QDateTime createdAt = QDateTime::currentDateTime();
    QString message = comment_author->getUi()->te_new_comment->toPlainText();
    int repoId = data->getRepo()->getId();

    QString str =QString("");
    emit comment_created(QSharedPointer<Comment>::create(type, parentId, createdAt, message, repoId), str);
    comment_author->done(0);

}


void SingleRepoPage::author_node_clicked(Author author)
{
    comment_author = new CommentOnAuthor();
    comment_author->open();
    this->author = author;

    connect(comment_author->getUi()->pb_author_comment, &QPushButton::clicked, this, &SingleRepoPage::pb_author_comment_clicked);

    if(this->author.getComments().size() > 0){
        QString tmp = "";
        for(auto &el : author.getComments()){
            tmp.append("• " + el->getMessage()).append("\n");
        }
        tmp = tmp.left(tmp.size()-1);
        qDebug() << tmp;
        comment_author->getUi()->te_old_comments->setText(tmp);
    } else{
        comment_author->getUi()->te_old_comments->setText("This author does not have any comments.");
    }
}


void SingleRepoPage::pb_commit_comment_clicked()
{
    TypeOfComm type = TypeOfComm::commit;
    QString parentId = commits->at(this->index_of_commit)->getId();
    QDateTime createdAt = QDateTime::currentDateTime();
    QString message = comment_commit->getUi()->pte_commit_comment->toPlainText();
    int repoId = data->getRepo()->getId();

    QString commEmail = QString(commits->at(this->index_of_commit)->getAuthor().getEmail());
    emit comment_created(QSharedPointer<Comment>::create(type, parentId, createdAt, message, repoId), commEmail);
    comment_commit->done(0);

    auto layout_commits = qobject_cast<QFormLayout*>(ui->commit_info_layout->layout());
    delete_layout_content(layout_commits);
    commit_node_clicked(this->commits);
}


void SingleRepoPage::pb_add_comment_for_commit_clicked(int i)
{
    comment_commit = new CommentOnCommit();
    comment_commit->open();
    this->index_of_commit = i-1;

    connect(comment_commit->getUi()->pb_commit_comment, &QPushButton::clicked, this, &SingleRepoPage::pb_commit_comment_clicked);
}


void SingleRepoPage::delete_layout_content(QFormLayout * layout)
{
    int rows = layout->rowCount();


    for(int i=0; i<rows; i++){
        layout->removeRow(0);
    }
}


void SingleRepoPage::pb_back_to_projects_clicked()
{
    ui->stackedWidget->setCurrentIndex(0);
    ui->cb_chose_timeline->setCurrentIndex(0);
    ui->scrollArea_commitInfo->setVisible(false);


    auto layout_repo = qobject_cast<QFormLayout*>(ui->repo_info_layout->layout());
    delete_layout_content(layout_repo);


    auto layout_commit = qobject_cast<QFormLayout*>(ui->commit_info_layout->layout());
    delete_layout_content(layout_commit);


    this->plot_timeline->delete_vertical_lines();
    this->plot_timeline->delete_months();
    this->plot_timeline->delete_commits();
    this->plot_timeline->delete_horizontal_lines_and_authors();

    check_box->deleteLater();
    plot_timeline->deleteLater();

    qDebug() << "_____________________________________";
}





