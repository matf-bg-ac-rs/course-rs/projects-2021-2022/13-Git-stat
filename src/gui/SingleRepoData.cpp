#include "include/gui/SingleRepoData.h"


SingleRepoData::SingleRepoData(QSharedPointer<Repo> repo)
    : repo(repo)
{
    qDebug() << "data je kreiran";
}

QVector<Author> SingleRepoData::getAuthors()
{
    return repo->getCommitsPerAuthor()->keys();
}

QSharedPointer<Repo> SingleRepoData::getRepo()
{
    return repo;
}

QVector<QSharedPointer<Commit>> SingleRepoData::findCommits(Author author)
{
    return repo->getCommitsPerAuthor()->value(author);
}

