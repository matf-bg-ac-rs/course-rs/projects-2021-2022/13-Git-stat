#include "include/gui/RepoNode.h"
#include "include/data/DataProcessing.h"
#include "ui_Repo.h"

#include <QToolTip>

RepoNode::RepoNode(QWidget *parent):
    QWidget(parent),
    ui(new Ui::RepoNode),
    dp(DataProcessing::getInstance())
{
    ui->setupUi(this);

    ui->frame->setObjectName("my");
    ui->frame->setStyleSheet("#my {border: 3px solid black; border-radius: 10px }");

    ui->pbDetails->setDisabled(true);
    ui->pbDetails->setToolTip("You can see more details about this repo when data is ready.");

    connect(ui->pbRefresh,&QPushButton::clicked,this,&RepoNode::pbRefreshRepositoryClicked);
    connect(ui->pbDetails,&QPushButton::clicked,this,&RepoNode::pbSeeMoreDetailsClicked);
    connect(ui->pbRemove,&QPushButton::clicked,this,&RepoNode::pbRemoveClicked);
}

RepoNode::~RepoNode()
{
    delete ui;
}

Ui::RepoNode *RepoNode::getUi() const
{
    return ui;
}

void RepoNode::pbRefreshRepositoryClicked()
{
    int id = this->getRepo()->getId();
    dp->getProjectInfo(id,true);
}

void RepoNode::pbSeeMoreDetailsClicked()
{
    emit seeMoreDetails(repo);
}

void RepoNode::pbRemoveClicked()
{
    emit removeRepository(this);
}

QSharedPointer<Repo> RepoNode::getRepo() const
{
    return repo;
}

void RepoNode::setRepo(QSharedPointer<Repo> newRepo)
{
    repo = newRepo;
}

void RepoNode::setWindow(MainWindow *w)
{
    this->w = w;
}

int RepoNode::getRow() const
{
    return row;
}

int RepoNode::getColumn() const
{
    return column;
}

void RepoNode::setColumn(int newColumn)
{
    column = newColumn;
}

void RepoNode::setRow(int newRow)
{
    row = newRow;
}

void RepoNode::setUrlAsLink(QString url)
{
    QString tekst("<a href=\"");
    tekst.append(url).append("\">");
    tekst.append(url).append("</a>");
    ui->tbUrl->setText(tekst);
    ui->tbUrl->setTextInteractionFlags(Qt::TextBrowserInteraction);
    ui->tbUrl->setOpenExternalLinks(true);
    ui->tbUrl->setAlignment(Qt::AlignCenter);
}

void RepoNode::setNode(){

    QString dateTime1 = this->repo->getCreatedAt().toString("yyyy-MM-dd hh:mm");
    QString dateTime2 = this->repo->getLastActivity().toString("yyyy-MM-dd hh:mm");

    ui->teName->setText(this->repo->getName());
    ui->teName->setAlignment(Qt::AlignCenter);
    this->setUrlAsLink(this->repo->getWebUrl());

    QString gr = this->repo->getGroupName();
    if(gr == ""){
        ui->lGroup->setText("");
    } else{
        QString grName("Group name:        ");
        grName.append(this->repo->getGroupName());
        ui->lGroup->setText(grName);
    }

    ui->lCreatedDate->setText(dateTime1);
    ui->lLastDate->setText(dateTime2);
    ui->lCommitsNumber->setText(QString::number(this->repo->getNumOfCommits()));
    ui->lAuthorsNumber->setText(QString::number(this->repo->getNumOfAuthors()));
}
