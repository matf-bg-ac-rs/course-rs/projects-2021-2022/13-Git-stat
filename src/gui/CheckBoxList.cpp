#include "include/gui/CheckBoxList.h"
#include "ui_MainWindow.h"

CheckBoxList::CheckBoxList(Ui::MainWindow* ui, QGraphicsScene* scene, QSharedPointer<SingleRepoData> data)
    : ui(ui)
    , scene(scene)
    , data(data)
{
    qDebug() << "check_box kreiran";
}

CheckBoxList::~CheckBoxList()
{
    qDebug() << "check_box je unisten";
}

void CheckBoxList::create_check_boxes()
{
    QScrollArea* scroll_area = ui->scrollArea_page_two;

    QWidget* container = new QWidget();
    QVBoxLayout* containerLayout = new QVBoxLayout();
    container->setLayout(containerLayout);
    scroll_area->setWidget(container);

    ui->lbl_3->setContentsMargins(0,13, 0, 0);

    int numAuthors = data->getAuthors().size();
    checkboxes.clear();
    for(int i=0; i<numAuthors; i++){
        checkboxes.append(new QCheckBox(data->getAuthors()[i].getName()));
        checkboxes[i]->setChecked(true);
        containerLayout->addWidget(checkboxes[i]);
    }
}

QVector<QCheckBox *> CheckBoxList::getCheckboxes()
{
    return checkboxes;
}

void CheckBoxList::pb_choose_clicked()
{
    QVector<bool> checked_fields;
    int numAuthors = data->getAuthors().size();

    for(int i=0; i<numAuthors; i++){
        if(checkboxes[i]->isChecked()){
            checked_fields.append(true);
        }
        else{
            checked_fields.append(false);
        }
    }

    emit authors_are_selected(checked_fields);
}



