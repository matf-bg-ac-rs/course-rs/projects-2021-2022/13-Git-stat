#include "include/gui/MonthNode.h"
#include <QPainter>


MonthNode::MonthNode()
    :QGraphicsItem()
{

}

MonthNode::MonthNode(QString name)
    :QGraphicsItem()
    ,name(name)
{

}

QRectF MonthNode::boundingRect() const
{
    return QRectF(0, 0, Width(), Height());
}

void MonthNode::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    painter->fillRect(boundingRect(), Qt::white);
    painter->setPen(Qt::black);

    const auto text = this->name;
    painter->drawText(boundingRect(), Qt::AlignHCenter | Qt::AlignVCenter, text);

}

QString MonthNode::getName()
{
    return this->name;
}

qint32 MonthNode::Height() const
{
    return 50;
}

qint32 MonthNode::Width() const
{
    return 90;
}
