#include "include/gui/MainWindow.h"
#include "include/gui/AllReposPage.h"
#include "include/gui/SingleRepoPage.h"

#include <QApplication>
#include <QObject>

void show_content_of_the_first_page(AllReposPage *po){
    po->showRepos();
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;

    AllReposPage *po = new AllReposPage(w.getUi(), &w);
    show_content_of_the_first_page(po);

    SingleRepoPage* pt = SingleRepoPage::getInstance();
    pt->setUi(w.getUi());
    pt->setScene(w.getScene());
    pt->connect_once();
    pt->select_duration_of_the_timeline();
    pt->draw_on_a_graphics_view();

    w.show();

    return a.exec();
}
