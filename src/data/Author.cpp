#include "../../include/data/Author.h"

#include <utility>

Author::Author(QString name, QString email, QVector<QSharedPointer<Comment>> comm){
    this->name=std::move(name);
    this->email=std::move(email);
    this->comments=std::move(comm);
}

Author::Author()=default;

QString Author::getName() const{
    return name;
}

QString Author::getEmail() const{
    return email;
}

QVector<QSharedPointer<Comment>> Author::getComments() const{
    return comments;
}

void Author::addComment(QSharedPointer<Comment>& comm){
    auto it = std::upper_bound(comments.cbegin() ,comments.cend(),comm,[](const QSharedPointer<Comment>& a, const QSharedPointer<Comment>& b) {return (a->getCreatedAt() < b->getCreatedAt() );} );
    this->comments.insert(it,comm);
}

void Author::setComments( QVector<QSharedPointer<Comment>> comm) {
    this->comments = std::move(comm);
}

