//
// Created by caca on 27.11.21..
//

#include "include/data/DataProcessing.h"
#include "include/gui/SingleRepoPage.h"
#include <string>
#include <QFile>
#include <QJsonDocument>
#include <QString>
#include <filesystem>
#include <utility>

DataProcessing* DataProcessing::instance{};

DataProcessing* DataProcessing::getInstance() {
    if(DataProcessing::instance == nullptr) {
        DataProcessing::instance = new DataProcessing();
    }
    return DataProcessing::instance;
}

DataProcessing::DataProcessing(QObject *parent):QObject(parent) {
    network = Network::GitLabNetworking::getInstance();
    page_two = SingleRepoPage::getInstance();
    repoProcessor = ProcessingRepo::getInstance();
    commentProcessor = ProcessingComments::getInstance();
    commitProcessor = ProcessingCommit::getInstance();

    connect(network, &Network::GitLabNetworking::downloadedProjectInfo, this, &DataProcessing::saveProjectInfo);
    connect(network, &Network::GitLabNetworking::downloadedProjectsInGroup, this, &DataProcessing::saveProjectsInGroup);
    connect(network, &Network::GitLabNetworking::downloadedCommitInfo, this, &DataProcessing::saveCommitInfo);

    connect(page_two, &SingleRepoPage::comment_created, this, &DataProcessing::addComment);
    connect(page_two, &SingleRepoPage::create_report, this, &DataProcessing::createReport);

    QString dirPath = QDir::homePath().append("/").append("Git-stat/").append("dataInfo");
    dataDir = QDir(dirPath);
    if (!dataDir.exists()) {
        if (dataDir.mkpath(".")) {
            qWarning() << "Uspesno kreiran direktorijum\n";
        } else {
            qWarning() << "Nije uspesno kreiran direktorijum\n";
            return;
        }
    }
    onDisk = repoProcessor->readAllReposFromDisk();
}

void DataProcessing::setFileNameForTests(QString& newPath){
    DataProcessing::repoPath = newPath;
    onDisk = repoProcessor->readAllReposFromDisk(repoPath);
}

void DataProcessing::removeRepo(int id){
    for(auto it=onDisk->cbegin();it!=onDisk->cend();it++){
        if(it->get()->getId() ==id){
            onDisk->erase(it);
            break;
        }
    }

    repoProcessor->writeAllReposToDisk(onDisk, repoPath);
    ProcessingComments::removeCommentsForRepoId(id);
}

bool DataProcessing::checkIfOnDrive(int id) const{
    return std::any_of(onDisk->begin(),onDisk->end(),[&id](const QSharedPointer<Repo>& element){return element->getId()==id; } );
}

void DataProcessing::getGroupInfo(int groupId) {
    network->getAllProjectsInGroup(groupId);
}

void DataProcessing::getProjectInfo(int repoId, bool needToRefresh) {
     if(needToRefresh || !checkIfOnDrive(repoId)) {
         DataProcessing::network->getProjectInfo(repoId);
     }else{
        for(const auto& el: *onDisk){
             if(el->getId()==repoId)
                 emit DataProcessing::readyProjectInfo(el);
         }
    }
}

void DataProcessing::saveProjectInfo(std::shared_ptr<Network::GitLabProjectResponse> projectInfo){
    auto repo = ProcessingRepo::parseToRepoProjectNetworkResponse(projectInfo);
    int id = repo->getId();
    bool found = false;
    int i = 0;
    for(auto it=onDisk->begin();it!=onDisk->end();++it){
        if(it->data()->getId() == id){
            onDisk->replace(i,repo);
            found = true;
            break;
        }
        i++;
    }

    if(!found){
        onDisk->push_back(repo);
    }
    repoProcessor->writeAllReposToDisk(onDisk);

    repo->setComments(ProcessingComments::getCommentsForObjectId(QString::number(id),TypeOfComm::repo,id));
    emit DataProcessing::readyProjectInfo(repo);
    DataProcessing::network->getCommitsForProject(id);
}

void DataProcessing::saveProjectsInGroup(std::shared_ptr<Network::GitLabGroupResponse> groupInfo){
    std::string groupName=groupInfo->name;
    std::vector<std::shared_ptr<Network::GitLabProjectResponse>> projects = groupInfo->projects;

    for(auto & project : projects) {
        int id = project->id;
        if(checkIfOnDrive(id)){
            for(const auto& el: *onDisk){
                if(el->getId()==id) {
                    emit DataProcessing::readyProjectInfo(el);
                    break;
                }
            }
            continue;
        }

        QSharedPointer<Repo> repo = ProcessingRepo::parseToRepoProjectNetworkResponse(project);
        onDisk->push_back(repo);

        repoProcessor->writeAllReposToDisk(onDisk, dataDir.absolutePath().append("/repos.json"));
        repo->setComments(ProcessingComments::getCommentsForObjectId(QString::number(id),TypeOfComm::repo,id));
        emit DataProcessing::readyProjectInfo(repo);
        network->getCommitsForProject(project->id);
    }
}

void DataProcessing::saveCommitInfo(int projectId, std::shared_ptr<std::list<std::shared_ptr<Network::GitLabCommitResponse>>> res ){
    QSharedPointer<QMap<Author, QVector<QSharedPointer<Commit>>>> sorted =QSharedPointer<QMap<Author, QVector<QSharedPointer<Commit>>>>::create();
  //  int numOfComm=0;
   // int numOfAuth=0;
    for(const auto &pom : *res){
        QSharedPointer<Commit> commit = commitProcessor->parseToCommitCommitNetworkResponse(projectId,pom);
        if(!sorted->contains(commit->getAuthor())){
   //         numOfAuth++;
            sorted->insert(commit->getAuthor(),QVector<QSharedPointer<Commit>>{commit});
        } else {
            auto it = std::upper_bound(sorted->find(commit->getAuthor())->begin() ,sorted->find(commit->getAuthor())->end(),commit,[](const QSharedPointer<Commit>& a, const QSharedPointer<Commit>& b) {return (a->getCreatedAt() < b->getCreatedAt() );} );
            sorted->find(commit->getAuthor())->emplace(it,commit);
        }
    }

    QSharedPointer<Repo> repo;
    for(const auto& el: *onDisk){
        if(el->getId()==projectId){
            repo=el;
            break;
        }
    }
    ProcessingRepo::setCommitStatsForRepo(repo,sorted);
    repoProcessor->writeAllReposToDisk(onDisk, dataDir.absolutePath().append("/repos.json"));
    emit readyWithCommits(repo);
}

void DataProcessing::addComment(const QSharedPointer<Comment>& comment, QString email){
    QSharedPointer<Repo> r = nullptr;
    for (const auto &pom: *onDisk) {
        if (pom->getId() == comment->getRepoId()) {
            r = pom;
            break;
        }
    }

    if (r == nullptr) {
        qWarning() << "Nije pronadjen repozitorijum u koji treba da se unese komentar";
        return;
    }
    commentProcessor->addCommentToRepo(r,comment, email);
    commentProcessor->writeCommentToDisk(comment);
}

void DataProcessing::setReposForReportTest(QSharedPointer<QVector<QSharedPointer<Repo>>> testRepo){
    onDisk=std::move(testRepo);
}

void DataProcessing::setFileNameForReportTest(QString& newPath){
    reportPath=newPath;
}

void DataProcessing::createReport(int repoId) const {
    QSharedPointer<Repo> r;
    for (auto &el: *onDisk) {
        if (el->getId() == repoId) {
            r = el;
            break;
        }
    }
    QString repoName = r->getName().split("/").last().trimmed();
    QString fileName = reportPath+repoName+".txt";
    QFile myFile(fileName);

    if (myFile.open(QIODevice::OpenModeFlag::WriteOnly | QIODevice::Text)) {
        QTextStream stream(&myFile);
        QString name = r->getName().split("/").last();
        int repo_id = r->getId();
        stream << name << " (" << QString::number(repo_id) << ")\n";
        if(!r->getComments().empty()) {
            stream<<"\tKomentari za repozitorijum: {\n";
            for (const auto &el: r->getComments()) {
                stream << "\t\t" << el->getMessage() << ";\n";
            }
            stream << "\t}\n";
        }
        QMap<Author, QVector<QSharedPointer<Commit>>>::iterator i;
        QSharedPointer<QMap<Author, QVector<QSharedPointer<Commit>>>> map = r->getCommitsPerAuthor();
        for (i = map->begin(); i != map->end(); ++i) {
            stream << "\n\n";
            stream << "\t" << i.key().getName() << " (br. komitova: " << i.value().size() << ") //"<< i.key().getEmail() << "\n";
            if(!i.key().getComments().empty()) {
                stream<<"\t\tKomentari za autora: {\n";
                for (const auto &el: i.key().getComments()) {
                    stream << "\t\t\t" << el->getMessage() << ";\n";
                }
                stream << "\t\t}\n";
            }
            for (const auto &el:i.value()) {
                stream << "\t\t";
                QString message = el->getMessage();
                QString pp = message.replace("\n\n", "\n\t\t\t");
                stream << el->getId() << ": " << pp;
                stream<<"\n";
                if(!el->getComments().empty()) {
                    stream<<"\t\t\tKomentari za komit: {\n";
                    for (const auto &pom: el->getComments()) {
                        stream << "\t\t\t\t" << pom->getMessage() << ";\n";
                    }
                    stream << "\t\t\t}\n";
                }
                stream<<"\n";
            }
        }
        myFile.close();
    }
}
