#include "../../include/data/Comment.h"

#include <utility>

Comment::Comment(const TypeOfComm &type, QString parentId, QDateTime createdAt, QString message, int repoId)
:typeComment(type),parentId(std::move(parentId)),createdAt(std::move(createdAt)),message(std::move(message)), repoId(repoId)
{}

QString Comment::typeToString() const {
    switch(this->typeComment){
        case TypeOfComm::author:
            return "author";
        case TypeOfComm::commit:
            return "commit";
        default:
            return "repo";
    }
}

TypeOfComm Comment::getTypeOfComm() const{
    return typeComment;
}

QString Comment::getParentId() const{
    return parentId;
}

QDateTime Comment::getCreatedAt() const{
    return createdAt;
}

QString Comment::getMessage() const{
    return message;
}

TypeOfComm Comment::getTypeOfComm(const QString& typeCom){
    if(typeCom=="author")
        return TypeOfComm::author ;
    if(typeCom=="commit")
        return TypeOfComm::commit;
    else
        return TypeOfComm::repo;
}

bool Comment::operator<(const Comment &comm) const {
    return(this->getCreatedAt() < comm.getCreatedAt());
}

bool Comment::operator==(const Comment &comm) const {
    return this->getCreatedAt() == comm.getCreatedAt() &&
            this->getMessage()==comm.getMessage() &&
            this->getRepoId() == comm.getRepoId() &&
            this->getParentId() == comm.getParentId() &&
            this->getTypeOfComm() == comm.getTypeOfComm();
}

int Comment::getRepoId() const{
    return this->repoId;
}
