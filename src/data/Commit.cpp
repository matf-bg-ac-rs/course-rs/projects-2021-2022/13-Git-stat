#include "../../include/data/Commit.h"

#include <utility>

Commit::Commit(QString id,QDateTime createdAt,
            QString title,
            QString message, Author author,
            QString webUrl, const Commit::CommitStats &stats, QVector<Commit::FileChange> fc, QVector<QSharedPointer<Comment>> comm)
            :id(std::move(id)),createdAt(std::move(createdAt)),title(std::move(title)),message(std::move(message)),author(std::move(author)),webUrl(std::move(webUrl)), stats(stats), changes(std::move(fc)), comments(std::move(comm))
            {
    }

Commit::~Commit()=default;

Commit& Commit::operator=(const Commit& commit)=default;
Commit::Commit(Commit& commit)=default;


const QString &Commit::getId() const{
    return id;
}

const QDateTime &Commit::getCreatedAt() const{
    return createdAt;
}

const QString &Commit::getTitle() const{
    return title;
}

const QString &Commit::getMessage() const{
    return message;
}

const Author &Commit::getAuthor() const {
    return author;
}

const QString &Commit::getWebUrl() const{
    return webUrl;
}

const Commit::CommitStats &Commit::getStats() const{
    return stats;
}

const QVector<Commit::FileChange> &Commit::getFileChanges() const{
    return changes;
}


Commit::FileChange::FileChange(Network::GitLabCommitResponse::FileChange &fc)
: oldFilename(QString::fromStdString(fc.oldFilename)), newFilename(QString::fromStdString(fc.newFilename)), isFileCreated(fc.isFileCreated),isFileDeleted(fc.isFileDeleted),isFileRenamed(fc.isFileRenamed)
{
}

Commit::FileChange::FileChange(QString old, QString newf, bool creat, bool rename, bool deleted)
:oldFilename(std::move(std::move(old))), newFilename(std::move(newf)), isFileCreated(creat), isFileRenamed(rename), isFileDeleted(deleted)
{}


QVector<QSharedPointer<Comment>> Commit::getComments() const{
    return comments;
}

void Commit::addComment(const QSharedPointer<Comment>& comm){
    auto it = std::upper_bound(comments.cbegin() ,comments.cend(),comm,[](const QSharedPointer<Comment>& a, const QSharedPointer<Comment>& b) {return (a->getCreatedAt() < b->getCreatedAt() );} );
    this->comments.insert(it,comm);
}

void Commit::setComment(QVector<QSharedPointer<Comment>> comm){
    this->comments.swap(comm);
}

bool Commit::FileChange::operator==(const Commit::FileChange &rhs) const {
    return oldFilename == rhs.oldFilename &&
           newFilename == rhs.newFilename &&
           isFileCreated == rhs.isFileCreated &&
           isFileRenamed == rhs.isFileRenamed &&
           isFileDeleted == rhs.isFileDeleted;
}

bool Commit::FileChange::operator!=(const Commit::FileChange &rhs) const {
    return !(rhs == *this);
}

bool Commit::CommitStats::operator==(const Commit::CommitStats &rhs) const {
    return additions == rhs.additions &&
           deletions == rhs.deletions &&
           total == rhs.total;
}

bool Commit::CommitStats::operator!=(const Commit::CommitStats &rhs) const {
    return !(rhs == *this);
}

bool Commit::operator==(const Commit &rhs) const {
    return id == rhs.id &&
           author == rhs.author &&
           createdAt == rhs.createdAt &&
           title == rhs.title &&
           message == rhs.message &&
           webUrl == rhs.webUrl &&
           stats == rhs.stats &&
           changes == rhs.changes &&
           comments == rhs.comments;
}

bool Commit::operator!=(const Commit &rhs) const {
    return !(rhs == *this);
}
