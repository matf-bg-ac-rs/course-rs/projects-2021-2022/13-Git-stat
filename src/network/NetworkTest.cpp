//
// Created by luka on 4.12.21..
//

#include <iostream>
#include "include/network/NetworkTest.h"
#include "include/network/GitLabProjectResponse.h"
#include "include/network/GitLabGroupResponse.h"

using namespace Network;

NetworkTest::NetworkTest() {
    connect(network, &Network::GitLabNetworking::downloadedProjectInfo,
            [=](std::shared_ptr<Network::GitLabProjectResponse> project){
                qWarning().noquote() << "Success!\n" << project.get();
            });
    connect(network, &Network::GitLabNetworking::downloadedCommitInfo,
            [=] (int projectId, const std::shared_ptr<std::list<std::shared_ptr<Network::GitLabCommitResponse>>> commits) {
                qWarning().noquote() << "Success! Project " << projectId << " retrieved.\n";
                for(auto && it : *commits) {
                    qWarning() << *it;
                }
            });
    connect(network, &Network::GitLabNetworking::downloadedProjectsInGroup,
            [=] (const std::shared_ptr<Network::GitLabGroupResponse> group) {
                qWarning().noquote() << "Success!\n" << *group;
            });
    connect(network, &GitLabNetworking::authCompleted,
            [=] (bool success, const std::string& text) {
        qWarning() << "Auth success:" << success;
        qWarning() << "Message:" << QString::fromStdString(text);
    });

    connect(network, &GitLabNetworking::rateLimited,
            [=] (int seconds) {
        qWarning() << "OOPS: Rate limited for" << seconds;
    });
    connect(network, &GitLabNetworking::allRequestsFinished,
            [=] () {
        qWarning() << "FINISHED ALL";
    });

    connect(network, &Network::GitLabNetworking::handleNetworkError,
            [=] (const std::string& msg) {
        qCritical() << "ERROR: " << msg.c_str();
    });
}

void NetworkTest::getGroup(int id) {
    network->getAllProjectsInGroup(id);
}

void NetworkTest::getProject(int id) {
    qDebug() << "NetworkTest: " << QThread::currentThreadId();
    network->getProjectInfo(id);
}

void NetworkTest::getCommits(int id) {
    network->getCommitsForProject(id);
}

void NetworkTest::auth(const std::string &token) {
    network->setAuthToken(token);
    QThread::msleep(1000);
}

