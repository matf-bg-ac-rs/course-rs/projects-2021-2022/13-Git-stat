//
// Created by luka on 27.12.21..
//

#include "include/network/GitLabResponseHandler.h"

using namespace Network;

void GitLabResponseHandler::handleProjectResponse(QNetworkReply* reply, const QJsonDocument& parsedResponse) {
    auto json = parsedResponse.object();
    auto obj = std::make_shared<GitLabProjectResponse>(json); //Qt doesn't like unique_ptr
    emit downloadedProjectInfo(obj);
}

void GitLabResponseHandler::handleCommitsListResponse(QNetworkReply* reply, const QJsonDocument& parsedResponse) {
    auto projectId = reply->property(PROP_REQUEST_ID).toInt();
    if(!projectContext->exists(projectId)) {
        qCritical() << "Cannot find projectId" << projectId << "in handleCommitsListResponse!";
        emit fireError("An unknown error has occurred. Please try again later.", projectId);
        return;
    }

    auto json = parsedResponse.array();
    projectContext->addCommitCount(projectId, json.size());
    if(reply->hasRawHeader("link")) {
        if(!followLinkNext(reply, projectId)) {
            projectContext->finishCommitCount(projectId);
        }
    } else {
        projectContext->finishCommitCount(projectId);
    }

    for(auto && it : json) {
        auto id = it.toObject()["id"].toString().toStdString();
        std::string urlStr = GET_PROJECTS_URL + std::to_string(projectId) + GET_COMMITS_URL_SUFFIX + id;
        networkRequester->doGetRequest(urlStr, &GitLabResponseHandler::handleCommitResponse, projectId, id);
    }
}

void GitLabResponseHandler::handleCommitResponse(QNetworkReply* reply, const QJsonDocument& parsedResponse) {
    auto projectId = reply->property(PROP_REQUEST_ID).toInt();
    auto json = parsedResponse.object();
    auto obj = std::make_shared<GitLabCommitResponse>(json);
    if(!projectContext->insertCommits(projectId, obj)) {
        qWarning() << "Failed to insert commit to project" << projectId;
    }

    std::string urlStr = GET_PROJECTS_URL + std::to_string(projectId) + GET_COMMITS_URL_SUFFIX + obj->id + "/diff";
    networkRequester->doGetRequest(urlStr, &GitLabResponseHandler::handleCommitDiffResponse, projectId, obj->id);
}

void GitLabResponseHandler::handleCommitDiffResponse(QNetworkReply* reply, const QJsonDocument& parsedResponse) {
    int projectId = reply->property(PROP_REQUEST_ID).toInt();
    auto commitId = reply->property(PROP_COMMIT_ID).toString();
    auto commit = projectContext->getCommit(projectId, commitId.toStdString());
    if(commit == nullptr) {
        qCritical() << "Failed to get commit" << commitId << "from project" << projectId;
        emit fireError("Unexpected error has occurred. Please try again later.", projectId);
        return;
    }

    auto json = parsedResponse.array();
    for(auto && it : json) {
        auto diffJson = it.toObject();
        commit->fileChanges.push_back(std::make_shared<GitLabCommitResponse::FileChange>(diffJson));
    }
    projectContext->incrementCommitDownloaded(projectId);
    if(projectContext->areAllCommitsDownloaded(projectId)) {
        finalizeProjectDownload(projectId);
    }
}

void GitLabResponseHandler::handleGroupProjectsResponse(QNetworkReply* reply, const QJsonDocument& parsedResponse) {
    int groupId = reply->property(PROP_REQUEST_ID).toInt();
    auto json = parsedResponse.array();
    for(auto && it : json) {
        auto projectJson = it.toObject();
        auto project = std::make_shared<GitLabProjectResponse>(projectJson);

        if(!groupContext->insertProject(groupId, project)) {
            qCritical() << "Failed to insert project" << project->id << "to group" << groupId;
        }
    }

    auto groupData = groupContext->get(groupId);
    if(reply->hasRawHeader("link")) {
        if(!followLinkNext(reply, groupId) && groupData != nullptr) {
            emit downloadedProjectsInGroup(groupData);
            groupContext->erase(groupId);
        }
    } else if(groupData != nullptr) {
        emit downloadedProjectsInGroup(groupData);
        groupContext->erase(groupId);
    }
}

void GitLabResponseHandler::handleGroupResponse(QNetworkReply* reply, const QJsonDocument& parsedResponse) {
    auto json = parsedResponse.object();
    auto group = std::make_shared<GitLabGroupResponse>(json); //Qt doesn't like unique_ptr
    if(!groupContext->setData(group)) {
        qCritical() << "Failed to set group context data for group" << *group;
        emit fireError("An unexpected error has occurred. Please try again later.", group->id);
    }

    std::string urlStr = GET_GROUP_URL + std::to_string(group->id) + "/projects";
    networkRequester->doGetRequest(urlStr, &GitLabResponseHandler::handleGroupProjectsResponse, group->id);
}

void GitLabResponseHandler::handleUserInfoResponse(QNetworkReply* reply, const QJsonDocument& parsedResponse) {
    QJsonObject obj = parsedResponse.object();
    networkContext->authToken = reply->request().rawHeader(QByteArray::fromStdString(TOKEN_HEADER_NAME)).toStdString();
    auto token = reply->property(PROP_AUTH_TOKEN).toString().toStdString();
    saveAuthToken(token);
    networkContext->authRequestInProgress = false;
    emit authCompleted(true, obj["username"].toString().toStdString());
}


bool GitLabResponseHandler::followLinkNext(QNetworkReply* reply, int requestId) {
    auto linkRaw = reply->rawHeader("link");
    auto links = parseLinkHeader(linkRaw);
    bool followedLink = false;
    for(auto && it : links) {
        if(it->params["rel"] == "next") {
            networkRequester->doGetRequest(it->uriReference.toStdString(),
                                           reply->property(PROP_HANDLER).value<ResponseHandlerFunc>(),
                                           requestId);
            followedLink = true;
        }
    }
    return followedLink;
}


void GitLabResponseHandler::finalizeProjectDownload(int projectId) {
    emit downloadedCommitInfo(projectId, projectContext->getCommits(projectId));
    projectContext->erase(projectId);
    if(projectContext->isEmpty()) {
        emit allRequestsFinished();
        networkContext->retriedRequests.clear();
    }
}

void GitLabResponseHandler::executeResponseHandler(const ResponseHandlerFunc& handler, QNetworkReply * reply, const QJsonDocument& parsedResponse) {
    (this->*handler)(reply, parsedResponse);
}

GitLabResponseHandler::GitLabResponseHandler(QObject *parent, const std::shared_ptr<GroupContext> &groupContext,
                                             const std::shared_ptr<ProjectContext> &projectContext,
                                             const std::shared_ptr<NetworkContext> &networkContext,
                                             NetworkRequester *networkRequester) : QObject(parent),
                                                                                   groupContext(groupContext),
                                                                                   projectContext(projectContext),
                                                                                   networkContext(networkContext),
                                                                                   networkRequester(networkRequester) {}
