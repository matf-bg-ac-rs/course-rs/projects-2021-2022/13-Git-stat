//
// Created by luka on 26.12.21..
//

#include "include/network/NetworkRequester.h"
#include "include/network/NetworkUtils.h"

#include <utility>

using namespace Network;

void NetworkRequester::doGetRequest(const std::string &urlString, const ResponseHandlerFunc& responseHandlerFunc, int requestId,
                                    const std::string &commitId) {
    if(networkContext->authRequestInProgress) {
        emit fireError("Authorization token is currently in progress. Please try again.", requestId);
        return;
    }

    auto request = makeQNetworkRequest(urlString, networkContext->authToken);
    auto reply = qNam -> get(*request);

    connect(reply, &QNetworkReply::finished, responseParser, &NetworkResponseParser::parseResponseSlot);
    reply->setProperty(PROP_REQUEST_ID, requestId);
    reply->setProperty(PROP_HANDLER, QVariant::fromValue(responseHandlerFunc));
    reply->setProperty(PROP_TIME_CREATED, (qlonglong)getUnixTimestamp());
    if(!commitId.empty()) {
        reply->setProperty(PROP_COMMIT_ID, QString::fromStdString(commitId));
    }
}

NetworkRequester::NetworkRequester(QObject *parent, std::shared_ptr<NetworkContext> networkContext,
                                   QNetworkAccessManager *qNam, NetworkResponseParser *responseParser) : QObject(
        parent), networkContext(std::move(networkContext)), qNam(qNam), responseParser(responseParser) {}
