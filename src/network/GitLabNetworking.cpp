//
// Created by luka on 22.11.21..
//

#include "include/network/GitLabNetworking.h"
#include "include/network/NetworkUtils.h"
#include <QJsonObject>
#include <QNetworkReply>
#include <QThread>
#include <utility>
#include <algorithm>

using namespace Network;


GitLabNetworking* GitLabNetworking::instance{};

GitLabNetworking* GitLabNetworking::getInstance() {
    if(GitLabNetworking::instance == nullptr) {
        GitLabNetworking::instance = new GitLabNetworking();
    }
    return GitLabNetworking::instance;
}

GitLabNetworking::GitLabNetworking(QObject *parent) : QObject(parent) {
    executionThread = new QThread();
    qNam = new QNetworkAccessManager();
    networkContext = std::make_shared<NetworkContext>();
    projectContext = std::make_shared<ProjectContext>();
    groupContext = std::make_shared<GroupContext>();
    networkContext->authToken = loadAuthToken();
    responseParser = new NetworkResponseParser(this, networkContext);
    networkRequester = new NetworkRequester(this, networkContext, qNam, responseParser);
    responseParser->setNetworkRequester(networkRequester);
    responseHandler = new GitLabResponseHandler(this, groupContext, projectContext, networkContext, networkRequester);

    init();
}


GitLabNetworking::GitLabNetworking(QObject *parent, QThread* executionThread, QNetworkAccessManager *qNam,
                                   NetworkResponseParser *responseParser, NetworkRequester *networkRequester,
                                   GitLabResponseHandler *responseHandler,
                                   std::shared_ptr<ProjectContext> projectContext,
                                   std::shared_ptr<GroupContext> groupContext,
                                   std::shared_ptr<NetworkContext> networkContext)
        : QObject(parent), executionThread(executionThread), qNam(qNam), responseParser(responseParser),
          networkRequester(networkRequester), responseHandler(responseHandler), projectContext(std::move(projectContext)),
          groupContext(std::move(groupContext)), networkContext(std::move(networkContext))
{
   init();
}

void GitLabNetworking::getProjectInfoImpl(int id) {
    std::string urlStr = GET_PROJECTS_URL + std::to_string(id);
    networkRequester->doGetRequest(urlStr, &GitLabResponseHandler::handleProjectResponse, id);
}

void GitLabNetworking::getAllProjectsInGroupImpl(int id) {
    if(groupContext->createOrGet(id) != nullptr) {
        emit handleNetworkError("Group " + std::to_string(id) + " is already downloading. Please wait...");
        return;
    }

    std::string urlStr = GET_GROUP_URL + std::to_string(id);
    networkRequester->doGetRequest(urlStr, &GitLabResponseHandler::handleGroupResponse, id);
}

void GitLabNetworking::getCommitsForProjectImpl(int id) {
    if(projectContext->createOrGet(id) != nullptr) {
        emit handleNetworkError("Group " + std::to_string(id) + " is already downloading. Please wait...");
        return;
    }

    std::string urlStr = GET_PROJECTS_URL + std::to_string(id) + GET_COMMITS_URL_SUFFIX;
    networkRequester->doGetRequest(urlStr, &GitLabResponseHandler::handleCommitsListResponse, id);
}

void GitLabNetworking::setAuthTokenImpl(const std::string &token) {
    QNetworkRequest request;
    QUrl url(QString::fromStdString(GET_USER_URL));
    request.setUrl(url);
    request.setRawHeader(QByteArray::fromStdString(TOKEN_HEADER_NAME), QByteArray::fromStdString(token));
    auto reply = qNam ->get(request); //todo make this use doGetRequest?
    connect(reply, &QNetworkReply::finished,
            this, &GitLabNetworking::handleCheckAuthTokenRequest);
    reply->setProperty(PROP_AUTH_TOKEN, QString::fromStdString(token));
}

void GitLabNetworking::handleCheckAuthTokenRequest() {
    auto* reply = dynamic_cast<QNetworkReply *>(sender());
    if(reply == nullptr) {
        networkContext->authRequestInProgress = false;
        qCritical() << "Got non-reply sender in handleUserInfoResponse slot!";
    } else {
        if(reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt() == 401) {
            networkContext->authRequestInProgress = false;
            emit authCompleted(false, "Invalid token! Please re-check and try again.");
            reply->deleteLater();
            return;
        }
    }
    reply->setProperty(PROP_HANDLER, QVariant::fromValue(&GitLabResponseHandler::handleUserInfoResponse));
    responseParser->parseResponse(reply);
}

void GitLabNetworking::fireError(std::string message, int requestId) {
    auto timeNow = getUnixTimestamp();
    projectContext->softErase(requestId);
    groupContext->softErase(requestId);
    if(networkContext->lastErrorFiredAt < timeNow - 60) {
        networkContext->lastErrorFiredAt = timeNow;
        emit handleNetworkError(std::move(message));
    }
}

void GitLabNetworking::init() {
    moveToThread(executionThread);
    executionThread->start();
    qNam->moveToThread(executionThread);
    responseParser->moveToThread(executionThread);
    networkRequester->moveToThread(executionThread);
    responseHandler->moveToThread(executionThread);

    connect(this, &GitLabNetworking::getProjectInfo, this, &GitLabNetworking::getProjectInfoImpl);
    connect(this, &GitLabNetworking::getAllProjectsInGroup, this, &GitLabNetworking::getAllProjectsInGroupImpl);
    connect(this, &GitLabNetworking::getCommitsForProject, this, &GitLabNetworking::getCommitsForProjectImpl);
    connect(this, &GitLabNetworking::setAuthToken, this, &GitLabNetworking::setAuthTokenImpl);

    connect(responseParser, &NetworkResponseParser::rateLimited, this, &GitLabNetworking::rateLimited);
    connect(responseParser, &NetworkResponseParser::handleResponse, responseHandler, &GitLabResponseHandler::executeResponseHandler);
    connect(responseParser, &NetworkResponseParser::fireError, this, &GitLabNetworking::fireError);

    connect(networkRequester, &NetworkRequester::fireError, this, &GitLabNetworking::fireError);

    connect(responseHandler, &GitLabResponseHandler::fireError, this, &GitLabNetworking::fireError);
    connect(responseHandler, &GitLabResponseHandler::allRequestsFinished, this, &GitLabNetworking::allRequestsFinished);
    connect(responseHandler, &GitLabResponseHandler::authCompleted, this, &GitLabNetworking::authCompleted);
    connect(responseHandler, &GitLabResponseHandler::downloadedCommitInfo, this, &GitLabNetworking::downloadedCommitInfo);
    connect(responseHandler, &GitLabResponseHandler::downloadedProjectInfo, this, &GitLabNetworking::downloadedProjectInfo);
    connect(responseHandler, &GitLabResponseHandler::downloadedProjectsInGroup, this, &GitLabNetworking::downloadedProjectsInGroup);
}
