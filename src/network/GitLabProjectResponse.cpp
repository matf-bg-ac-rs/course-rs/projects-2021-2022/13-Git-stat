//
// Created by luka on 9.12.21..
//

#include "include/network/GitLabProjectResponse.h"


#define JSON2STR(key) json[key].toString().toStdString()
Network::GitLabProjectResponse::GitLabProjectResponse(QJsonObject &json) {
    id = json["id"].toInt();
    description = JSON2STR("description");
    name = JSON2STR("name");
    nameWithNamespace = JSON2STR("name_with_namespace");
    path = JSON2STR("path");
    pathWithNamespace = JSON2STR("path_with_namespace");
    createdAt = JSON2STR("created_at");
    defaultBranch = JSON2STR("default_branch");
    for(auto && it : json["tag_list"].toArray())
        tagList.push_back(it.toString().toStdString());
    for(auto && it : json["topics"].toArray())
        tagList.push_back(it.toString().toStdString());
    sshUrlToRepo = JSON2STR("ssh_url_to_repo");
    httpUrlToRepo = JSON2STR("http_url_to_repo");
    webUrl = JSON2STR("web_url");
    avatarUrl = JSON2STR("avatar_url");
    forksCount = json["forks_count"].toInt();
    starCount = json["star_count"].toInt();
    lastActivityAt = JSON2STR("last_activity_at");

    if(json.contains("namespace") && json["namespace"].toObject()["kind"] == GROUP_KIND_PROJECT) {
        QJsonObject namespaceObj = json["namespace"].toObject();
        groupName = namespaceObj["name"].toString().toStdString();
        groupId = namespaceObj["id"].toInt();
    }
}

Network::GitLabProjectResponse::operator QString() const {
    QString str(("GitLabProjectResponse{id: " + std::to_string(id) + ", description: " + description + ", name: "
                 + name + ", nameWithNamespace: " + nameWithNamespace + ", path: " + path + ", pathWithNamespace: "
                 + pathWithNamespace + ", createdAt: " + createdAt + ", defaultBranch: " + defaultBranch + ", tags_len: "
                 + std::to_string(tagList.size()) + ", topics_len: " + std::to_string(topics.size())
                 + ", sshUrlToRepo: " + sshUrlToRepo + ", httpUrlToRepo: " + httpUrlToRepo + ", webUrl: " + webUrl
                 + ", avatarUrl: " + avatarUrl + ", groupName: " + groupName + ", groupId: " + std::to_string(groupId)
                 + ", forksCount: " + std::to_string(forksCount) + ", starCount: " + std::to_string(starCount)
                 + ", lastActivityAt: " + lastActivityAt + "}").c_str());
    return str;
}

#undef JSON2STR
