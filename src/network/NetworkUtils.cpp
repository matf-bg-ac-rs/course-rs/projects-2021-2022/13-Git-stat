//
// Created by luka on 27.11.21..
//

#include <QString>
#include <QRegularExpression>
#include "include/network/NetworkUtils.h"

using namespace Network;

std::vector<std::shared_ptr<LinkHeader>> Network::parseLinkHeader(QByteArray& header) {
    QString headerStr = QString(header);
    std::vector<std::shared_ptr<LinkHeader>> links;
    QRegularExpression commaWhitespace = QRegularExpression("\\s*,\\s*");
    QRegularExpression semicolonWhitespace = QRegularExpression("\\s*;\\s*");
    auto linkRefs = headerStr.split(commaWhitespace);
    for(auto && it : linkRefs) {
        auto link = std::make_shared<LinkHeader>();
        auto linkRefParts = it.split(semicolonWhitespace);
        auto linkUri = linkRefParts[0];
        linkUri = linkUri.remove(linkUri.size()-1, 1);
        linkUri = linkUri.remove(0, 1);
        link->uriReference = linkUri;
        for(int i=1; i<linkRefParts.size(); i++) {
            auto linkParam = linkRefParts[i];
            auto linkParamKeyValue = linkParam.split('=');
            auto linkParamKey = linkParamKeyValue[0];
            auto linkParamValue = linkParamKeyValue[1];
            if(linkParamValue.startsWith("\"") && linkParamValue.startsWith("\"")) {
                //we're basically ignoring quoting rules here, as they're not really important for our case (for now)
                linkParamValue = linkParamValue.remove(linkParamValue.size()-1, 1);
                linkParamValue = linkParamValue.remove(0, 1);
            }
            link->params[linkParamKey] = linkParamValue;
            links.push_back(link);
        }
    }
    return links;
}

void Network::saveAuthToken(const std::string& token, const QString& path) {
    QFile authFile = QFile(path);
    if(authFile.open(QIODevice::OpenModeFlag::WriteOnly | QIODevice::Truncate)) {
        authFile.write(token.c_str());
        authFile.close();
    } else {
        auto errMsg = authFile.errorString();
        qCritical() << "Error making auth file for save token: " << errMsg;
    }
}

std::string Network::loadAuthToken(const QString& path) {
    QFile authFile = QFile(path);
    if(!authFile.exists()) return "";
    if(authFile.open(QIODevice::OpenModeFlag::ReadOnly)) {
        auto authToken = authFile.readAll().toStdString();
        authFile.close();
        return authToken;
    } else {
        auto errMsg = authFile.errorString();
        qCritical() << "Error making auth file for load token: " << errMsg;
        return "";
    }
}

std::string Network::mapErrorMessage(const QNetworkReply *reply) {
    switch (reply->error()) {
        case QNetworkReply::ContentNotFoundError:
            return "You have entered a non-existing id.";
        case QNetworkReply::AuthenticationRequiredError:
        case QNetworkReply::ContentOperationNotPermittedError:
            return "Authorization token doesn't exist or is invalid.";
        case QNetworkReply::ContentAccessDenied:
            return "Access denied. Check if token has read_api scope.";
        default:
            return "An unknown error occurred while downloading the project.";
    }
}

bool Network::isStatusRateLimited(const QVariant& statusCode) {
    return statusCode.toInt() == 429;
}

std::chrono::duration<long>::rep Network::getUnixTimestamp() {
    return std::chrono::duration_cast<std::chrono::seconds>(
            std::chrono::system_clock::now().time_since_epoch()).count();
}

std::shared_ptr<QNetworkRequest> Network::makeQNetworkRequest(const std::string& urlString, const std::string& authToken) {
    auto request = std::make_shared<QNetworkRequest>();
    QString urlQStr = QString::fromStdString(urlString);
    if(!urlQStr.contains("?")) {
        urlQStr = urlQStr.append("?per_page=100");
    }
    QUrl url(urlQStr);
    request->setUrl(url);
    request->setAttribute(QNetworkRequest::HttpPipeliningAllowedAttribute, true);

    if(!authToken.empty()) {
        request->setRawHeader(QByteArray::fromStdString(TOKEN_HEADER_NAME), QByteArray::fromStdString(authToken));
    }

    return request;
}

std::string Network::makeUniqueId(QNetworkReply* reply) {
    return (reply->property(PROP_REQUEST_ID).toString() + reply->property(PROP_COMMIT_ID).toString()).toStdString();
}

bool Network::shouldErrorBeRetried(QNetworkReply* reply) {
    if(!reply->error()) return false;

    auto statusCode = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
    return statusCode < 400 || statusCode >= 500;
}