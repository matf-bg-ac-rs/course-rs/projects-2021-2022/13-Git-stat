//
// Created by luka on 26.12.21..
//

#include "include/network/NetworkContext.h"

void Network::NetworkContext::populateRateLimitDataFromReply(QNetworkReply *reply) {
    if(reply->hasRawHeader("ratelimit-remaining") && reply->hasRawHeader("ratelimit-reset")
       && reply->hasRawHeader("ratelimit-resettime")) {
        auto resetTimeHeader = reply->rawHeader("ratelimit-resettime");
        rateLimitRemaining = reply->rawHeader("ratelimit-remaining").toInt();
        rateLimitResetsAt = reply->rawHeader("ratelimit-reset").toInt();
        qDebug() << "RATELIMIT: Remaining " << rateLimitRemaining << " requests until " << resetTimeHeader;
    }
}