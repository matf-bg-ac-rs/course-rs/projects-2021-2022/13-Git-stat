//
// Created by luka on 9.12.21..
//

#include "include/network/GitLabGroupResponse.h"

#define JSON2STR(key) json[key].toString().toStdString()
Network::GitLabGroupResponse::GitLabGroupResponse(QJsonObject &json) {
    id = json["id"].toInt();
    webUrl = JSON2STR("web_url");
    name = JSON2STR("name");
    path = JSON2STR("path");
    description = JSON2STR("description");
    avatarUrl = JSON2STR("avatar_url");
    fullName = JSON2STR("full_name");
    fullPath = JSON2STR("full_path");
    createdAt = JSON2STR("created_at");
    parentId = json["parent_id"].toInt();
}

Network::GitLabGroupResponse::operator QString() const {
    QString str(("GitLabGroupResponse{id: " + std::to_string(id) + ", webUrl: " + webUrl + ", name: " + name
                 + ", path: " + path + ", description: " + description + ", avatarUrl: " + avatarUrl
                 + ", fullName: " + fullName + ", fullPath: " + fullPath + ", createdAt: "
                 + createdAt + ", parentId: " + std::to_string(parentId) + ", projects: [\n\t").c_str());
    for(auto && it : projects) {
        str.append(*it);
        str.append(",\n\t");
    }
    str.append("]}");
    return str;
}

#undef JSON2STR

